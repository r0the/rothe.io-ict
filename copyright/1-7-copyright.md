# Urheberrecht
---

::: exercise Aufgabe

Lies den folgenden «Der Bund»-Artikel:

* [:link: Das Urheberrecht treibt die Netzgemeinde auf die Strasse][2]

Diskutiert in Gruppen über den Artikel. Beantwortet dabei folgende Fragen:

- Um welche Rechte geht es genau?
- Wieso ist der «Artikel 13» so umstritten?
- Was haltet ihr davon?
:::

## Werke

Das Urheberrecht regelt den Umgang mit **Werken**, beispielsweise

- Texte
- Musikstücke
- Filme
- Bilder und Fotografien
- Computerspiele
- Programme, Apps

## Erlaubte Nutzung

Grundsätzlich ist für jede Nutzung eines urheberrechtlich geschützten Werks eine **Erlaubnis** nötig. Das Gesetz erlaubt folgendes:

- Privatgebrauch
- Zitieren kleiner Textausschnitte
- Verwendung im Schulunterricht

Für jede weitergehende Nutzung muss die Erlaubnis des Rechteinhabers eingeholt werden. Urheber können mit **Lizenzen** automatisch zusätzliche Nutzungen erlauben.

::: exercise Aufgabe

Besprecht in Zweiergruppen die unten beschriebenen Situationen bezüglich Urheberrecht. Benutzt dazu die folgende Broschüre:

* [:download: Urheberrecht und verwandte Schutzrechte][1]

Ist die Nutzung gemäss Urheberrecht erlaubt oder nicht? Begründet Eure Antwort.

1. Du hörst auf Deinem Mobiltelefon ein aktuelles Musikstück auf Youtube.
2. Eine BG-Lehrerin verwendet ein von dir gemaltes Bild für einen Flyer.
3. Ein Lehrer lädt die ganze Klasse nach dem Unterricht zu einem Filmabend im Schulzimmer ein. Er lässt die Schülerinnen und Schüler auf seinem Netflix-Konto einen Film auswählen.
4. Du verwendest ein mit Google gefundenes Foto für einen Vortrag im Geografieunterricht.
5. Du lädst ein Foto, das du von einer Kollegin per WhatsApp erhalten hat, auf dein öffentliches Instagram-Konto hoch.
6. Du lädst ein mit Google gefundenes Foto auf dein privates Instagram-Konto hoch.
7. Du schaust die neueste Folge deiner Lieblingsserie auf Youtube.
8. Du lädst die neueste Folge deiner Lieblingsserie via Torrent herunter.
9. Eine Aufnahme der Big Band-Aufführung wird auf die Webseite der Schule gestellt.
:::

[1]: https://www.ige.ch/fileadmin/user_upload/schuetzen/urheberrecht/d/Urheberrecht-und-verwandte-Schutzrechte.pdf
[2]: https://www.derbund.ch/24403218
