# Bildersuche
---

## Quellen für wiederverwendbare Bilder

Auf folgenden Seiten kann nach wiederverwendbaren Bildern gesucht werden:

::: cards 2
[![](images/screenshot-pixabay.png)][1]
#### [Pixabay][1]
Pixabay bietet ausschliesslich Bilder an, die wiederverwendet werden dürfen. Für den Download der Bilder in Originalqualität muss ein Konto erstellt werden.

***
[![](images/screenshot-wikimedia-commons.png)][2]
#### [Wikimedia Commons][2]
Wikimedia Commons ist das Bildarchiv von Wikipedia und enthält viele Bilder, die wiederverwendet werden dürfen.

***
[![](images/screenshot-flickr.png)][3]
#### [Flickr][3]
Flickr ist eine grosse Foto-Website, auf welcher auch viele wiederverwendbare Bilder gefunden werden können.

***

[![](images/screenshot-google.png)][4]
#### [Google][4]
Die Google-Bildersuche kann auf wiederverwendbare Bilder eingeschränkt werden.
:::

## Suche nach wiederverwendbaren Bildern

### bei Google

Gehe folgendermassen vor, um die Google-Bildersuche auf wiederverwendbare Bilder einzuschränken:

- Klicke auf _Tools_.
- Klicke auf _Nutzungsrechte_.
- Wähle _Zur Wiederverwendung gekennzeichnet_ aus.

![](images/google-cc-search.svg)

### bei Flickr

Gehe so vor, um bei Flickr nach wiederverwendbaren Bildern zu suchen:

- Klicke auf _Beliebige Lizenz_.
- Wähle _Alle Creative Commons_ aus.

![](images/flickr-cc-search.svg)

[1]: https://pixabay.com
[2]: https://commons.wikimedia.org
[3]: https://www.flickr.com
[4]: https://www.google.ch/imghp
