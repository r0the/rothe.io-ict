# Bilder veröffentlichen
---

Bei der Veröffentlichung eines Bildes müssen verschiedene Rechte beachtet werden.

## Urheberrecht

Nach Urheberrecht gelten Bilder normalerweise als Werke. Die Urheberin oder der Urheber muss einer weiteren Veröffentlichung zustimmen, auch wenn das Bild schon öffentlich verfügbar ist.

## Persönlichkeitsrecht
