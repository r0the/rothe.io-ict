# Creative Commons
---

Creative Commons (CC) ist eine Organisation, welche verschiedene Standard-Lizenzverträge veröffentlicht. Mit diesen Lizenzen können Autoren der Öffentlichkeit Nutzungsrechte an ihren Werken einräumen.

## CC-Lizenzbausteine

Die Creative Commons-Lizenzen setzen sich aus vier verschiedenen Bausteinen zusammen:

::: cards 2
![](images/cc-by-example.png)
#### Namensnennung (BY)

Der Name des Urhebers muss erwähnt werden.
***
![](images/cc-nd-example.png)
#### Keine Bearbeitung (ND)

Das Werk darf nicht abgeändert werden .
***
![](images/cc-nc-example.png)
#### Nicht kommerziell (NC)

Das Werk darf nicht kommerziell genutzt werden.
***
![](images/cc-sa-example.png)
#### Weitergabe unter gleichen Bedingungen (SA)

Ein abgeändertes Werk muss unter der gleichen Lizenz veröffentlicht werden.
:::

Das folgende Video erklärt die Creative Commons-Lizenzen:

<video controls>
  <source src="https://media.mygymer.ch/rothe.io/creative-commons-kiwi.mp4" type="video/mp4">
</video>
