# Formatvorlagensatz wählen
---

Um das Aussehen deines Dokuments zu ändern, kannst du einen anderen **Formatvorlagensatz** auswählen. Im Menü _Entwurf_ findest du eine Auswahl von _Formatvorlagensätzen_:

![Formatvorlagensatz wählen](./word-style-set.png)

Durch einen Klick kannst du den entsprechenden Satz aktivieren.
