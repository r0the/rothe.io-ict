# Inhaltsverzeichnis
---

## Inhaltsverzeichnis einfügen

Um in Microsoft Word ein Inhaltsverzeichnis einzufügen, wird der Menüpunkt _Referenzen ‣ Inhaltsverzeichnis_ verwendet:

![Inhaltsverzeichnis einfügen in Microsoft Word](./word-table-of-contents-1.png)

Es stehen verschiedene Layouts zur Auswahl. Im Zweifelsfall wird das oberste Layout gewählt.

Im Inhaltsverzeichnis erscheinen automatisch alle Überschriften, also alle Absätze, welchen eine «Überschrift»-Formatvorlage zugewiesen wurde.

## Inhaltsverzeichnis aktualisieren

Das Inhaltsverzeichnis wird nicht automatisch aktualisiert, wenn neue Überschriften eingefügt werden oder die Seitenzahlen sich ändern. Gehe so vor, um das Inhaltsverzeichnis zu aktualisieren:

1. Inhaltsverzeichnis anklicken
2. Auf _Inhaltsverzeichnis aktualisieren…_ klicken.
3. _Gesamtes Verzeichnis aktualisieren_ auswählen.
4. Auf _OK_ klicken.

![Inhaltsverzeichnis aktualisieren in Microsoft Word](./word-table-of-contents-2.svg)
