# Formatierungszeichen
---

## Formatierungszeichen anzeigen

Textdokumente enthalten Formatierungszeichen. Diese werden standardmässig nicht angezeigt und selbstverständlich auch nicht ausgedruckt, steuern aber den Textfluss. Man kann sich diese versteckten Zeichen zur besseren Orientierung anzeigen lassen: _Start ‣ Absatz ‣ ¶_.

![Formatierungszeichen ein- und ausblenden](./word-formatting-marks.png)

## Die wichtigsten Formatierungszeichen


| Zeichen                 | Eingabe                                    |               Darstellung |
|:----------------------- |:------------------------------------------ | -------------------------:|
| Leerzeichen             | [Space]                                    |              __&middot;__ |
| geschütztes Leerzeichen | [Ctrl]+[Shift]+[Space] / [Command]+[Space] |                     __°__ |
| Zeilenumbruch           | [Shift]+[Enter]                            |               __&#8629;__ |
| Absatzwechsel           | [Enter]                                    |                     __¶__ |
| Seitenumbruch           | [Ctrl]+[Enter] / [Command]+[Enter]         | __--- Seitenumbruch ---__ |
| Tabulator               | [Tab]                                      |              __&#10141;__ |

Ein **geschütztes Leerzeichen** ist ein Leerzeichen, bei dem die Zeile nicht umgebrochen werden darf. Es wird beispielsweise in Telefonnummern oder bei Massangaben wie «CHF&nbsp;10.–» oder «12&nbsp;kg» verwendet, damit der Wert nicht von der Einheit getrennt wird.
