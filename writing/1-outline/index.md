# 1 Textgliederung
---

> «Das Aufteilen eines Textes in mehrere, meist hierarchisch strukturierte Teile wird als Gliederung bezeichnet. Eine Gliederung kann den Text übersichtlicher machen und es erleichtern, Informationen zu Teilaspekten schnell zu finden. Gerade bei wissenschaftlichen Texten ist eine Gliederung gebräuchlich.»[^1]

Textverarbeitungsprogramme unterscheiden **Zeichen**, **Absätze** und grössere Passagen, sogenannte **Abschnitte**. Sätze haben für Textverarbeitungsprogramme keine grosse Bedeutung.

## Gliederungselemente

### Absatz

Dies hier ist ein Absatz. Ein Absatz umfasst meist nur einen Gedankengang, ein Argument oder ein kleines Thema. Mehrere verschiedene Gedankengänge, Argumente oder Themen werden in der Regel in mehrere Absätze unterteilt.

Ein Absatzwechsel wird durch die Eingabetaste [Enter] erzeugt. Das ist nicht zu verwechseln mit einem manuellen Zeilenumbruch, welcher durch [Shift]+[Enter] erzeugt wird.

Optisch werden Absätze üblicherweise durch einen grösseren Abstand voneinander getrennt. Im englischen Sprachraum wird hingegen die erste Zeile von jedem Absatz eingerückt.

Zur besseren Übersicht können in einigen Textverarbeitungsprogrammen zusätzlich Absatzmarkensymbole «¶» am Ende der Absätze [eingeblendet werden](?page=7-formatting-marks/).

### Kapitel

Kapitel und Unterkapitel werden jeweils durch eine Überschrift eingeleitet. Aus Sicht der Textverarbeitungsprogramms sind Überschriften Absätze, welche durch das Zuweisen einer bestimmten Formatvorlage speziell markiert werden.

Durch diese Markierung kennt das Textverarbeitungsprogramm die Kapitelstruktur und kann beispielsweise automatisch ein Inhaltsverzeichnis erzeugen.

::: warning Guter Stil
Wenn der Abstand zwischen den Absätzen zu klein ist, sollte der Abstand entsprechend grösser eingestellt werden. Auf keinen Fall sollten leere Absätze («Leerzeilen») verwendet werden.
:::

### Abschnitt

Abschnitte unterteilen ein Dokument in Bereiche, in welchen das Seitenlayout unterschiedlich festgelegt werden kann. So können pro Abschnitt folgende Formatierungen festgelegt werden:

- Ausrichtung und Grösse der Seiten
- Seitenränder
- Anzahl Spalten
- unterschiedliche Kopf- und Fusszeilen

## Formatvorlagen

In Textverarbeitungsprogrammen wird die Gliederung mittels **Formatvorlagen** definiert. Im folgenden Beispiel wird das Dokument durch die Verwendung der Formatvorlage «Überschrift 1»  in drei Kapitel gegliedert:

![Gliederung eines Dokuments mittels Formatvorlagen](./outline.svg)


[^1]: Wikipedia: Gliederung, 2019, [https://de.wikipedia.org/wiki/Gliederung][1] (abgerufen am 3.3.2019)

[1]: https://de.wikipedia.org/wiki/Gliederung
