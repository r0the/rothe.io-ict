# Formatvorlagen zuweisen
---

## Seitenleiste «Formatvorlagen» einblenden

![Menübereich «Formatvorlagen»](./word-style-1.svg)

Du kannst auch die Seitenleiste für Formatvorlagen verwenden, welche einen besseren Überblick bietet. Diese Seitenleiste kann so angezeigt werden (siehe oben):

- **Windows:** Klicke auf die untere rechte Ecke im Menübereich _Formatvorlagen_.
- **macOS:** Klicke auf den Knopf _Bereich Formatvorlagen_.

![Seitenleiste «Formatvorlagen»](./word-style-2.gif)

## Formatvorlage zuweisen

Gehe folgendermassen vor, um in Microsoft Word einem Absatz eine Formatvorlage zuzuweisen:

- Klicke auf den Text, dem du eine Formatvorlage zuweisen möchtest.
- Wähle die entsprechende Formatvorlage aus.

![Seitenleiste «Formatvorlagen»](./word-style-3.gif)

## Weitere Einstellungen

Weitere Einstellungen können über _Optionen…_ erreicht werden. Hier lässt sich z.B. einstellen, ob die empfohlenen, die im Dokument verwendeten oder alle verfügbaren Formatvorlagen angezeigt werden sollen.

![Optionen für Formatvorlagen](./word-style-3.png)
