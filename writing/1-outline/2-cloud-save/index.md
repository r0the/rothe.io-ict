# Speichern in Cloud
---

Word speichert Dokumente normalerweise im DOCX-Format. Um eine Word-Datei in der Schul-Cloud zu speichern gehst du so vor:

1. Wähle den Menüpunkt _Datei ‣ Speichern unter_.
2. Wähle den Ort _OneDrive - Gymnasium Kirchenfeld_.
3. Wähle den gewünschten Ordner aus.
4. Gib den Dateinamen ein.
5. Klicke auf _Speichern_.

![](./word-save-as.svg)
