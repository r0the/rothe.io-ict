# Formatvorlagen ändern
---

Formatvorlagen können geändert werden, um so das Aussehen aller Absätze mit der entsprechenden Vorlage zu beeinflussen. Es gibt zwei Ansätze, eine Formatvorlage anzupassen:

1. Einen Absatz ändern und anschliessend die Vorlage aktualisieren.
2. Direkt die Vorlage ändern.

## Vorlage aktualisieren

Zuerst wird der Absatz nach Wunsch angepasst. Anschliessend klickt in der Formatvorlagen-Seitenleiste auf man auf den Dropdown-Pfeil der entsprechenden Vorlage und wählt den Menüpunkt _… aktualisieren, um der Auswahl anzupassen_.

![](./word-style-change.png)

## Vorlage ändern

Klicke in der Formatvorlagen-Seitenleiste auf den Dropdown-Pfeil der zu ändernden Formatvorlage und wähle den Menüpunkt _Ändern…_ (siehe Bild oben).

Im folgenden Fenster kann die Schrift, die Textausrichtung, der Zeilenabstand sowie der Absatzabstand der Formatvorlage angepasst werden. Anhand des Beispieltexts ist gleich sichtbar, was die Änderung bewirkt.

![](./word-style-change-2.svg)

Sobald auf _OK_ geklickt wird, werden die Änderungen übernommen und alle Absätze mit der entsprechnden Formatvorlage automatisch angepasst.

::: warning Änderung der Formatvorlage «Standard»
Alle Formatvorlagen basieren auf «Standard». Wenn in der Formatvorlage «Standard» eine Einstellung geändert wird, so beeinflusst dies auch sämtliche anderen Formatvorlagen.
:::
