# 5 Quellen verwenden
---

* [5.1 Suchmaschinen](?page=5-search/1-search-engine/)
* [5.2 :extra: Zusatzinformationen](?page=5-search/2-search-extra/)
* [5.3 Quellenbewertung](?page=5-search/3-evaluate/)
* [5.4 Quellenangabe](?page=5-4-citation)
