::: box exercise
#### :exercise: Aufgabe

Wähle eine der folgenden Aussagen und überprüfe sie mit einer Recherche:

- «Bei der Herstellung von iPhones werden Kinder ausgebeutet.»

Gehe für die gewählte Aussage folgendermassen vor:

1. Schreibe dir mindestens fünf sinnvolle Suchbegriffe auf.
2. Suche nach informativem Material. Wähle zwei Quellen aus, welche die Behauptung unterstützen und zwei Quellen, welche widersprechen.
3. Führe für die vier Quellen eine Quellenbewertung durch.
4. Ordne die Quellen nach Vertrauenswürdigkeit.
:::
