# :extra: Aufgabe Informatiker*in
---

::: box exercise
#### :extra: Aufgabe – Informatiker*in kennenlernen

Wähle eine wichtige Person aus der Informatik, beispielsweise eine der folgenden:

<div class="columns">
<div class="column is-6">

**Frauen aus der Informatik**
- Ada Lovelace
- Constanze Kurz
- Grace Hopper
- Jean Bartik
- Limor Fried
- Jeri Ellsworth
- Marissa Mayer
- Radia Perlman
- Yōko Shimomura
- Dorothy Vaughan
- Hedy Lamarr

</div><div class="column is-6">

**Männer aus der Informatik**
- Donald Knuth
- Konrad Zuse
- Linus Torvalds
- Niklaus Wirth
- Noam Chomsky
- Shigeru Miyamoto
- Steve Jobs
- Tim Berners-Lee

</div></div>

Nimm dir 5 bis 10 Minuten Zeit, um Informationen über diese Person zu sammeln.
:::

::: box exercise
#### :extra: Aufgabe – Kurzportrait schreiben
1. Schreibe ein Kurzportrait (ca. eine halbe Seite) der wichtigen Person aus der Informatik, die du gewählt hast. Der Bericht soll folgendes enthalten:

   - Titel
   - Untertitel
   - Überschriften
   - Text
   - Bild

   Das Kurzportrait soll etwa den gleichen Umfang wie dasjenige von Alan Turing haben.

2. Wähle einen passenden Formatvorlagensatz für dein Kurzportrait.

3. Erstelle eine PDF-Datei deines Kurzportraits.
:::
