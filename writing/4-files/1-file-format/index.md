# 4.1 Dateiformate
---

Word kann Dokumente in verschiedenen Dateiformaten speichern. Die wichtigsten Dateiformate sind folgende:

- **Word-Dokument (.docx):** Dies ist das «normale» Dateiformat für Word-Dokumente. Normalerweise sollte dieses Dateiformat verwendet werden.

- **Word 97-2003-Dokument (.doc):** Dies ist ein veraltetes Dateiformat für Word-Dokumente. Es sollte nicht mehr verwendet werden, da solche Dateien Viren enthalten können. Allerdings gibt es noch viele Word-Dokumente, welche in diesem Dateiformat vorliegen. Man kann solche Dokumente in das aktuelle Format konvertieren (siehe unten).

- **PDF (.pdf):** Das *Portable Document Format* ist «elektronisches Papier». Dieses Format wird verwendet, wenn man jemandem ein fertiges Dokument zusenden will.

- **OpenDocument-Text (.odt):** Dies ist ein alternatives Dateiformat, welches unter anderem vom Officepaket *LibreOffice* verwendet wird.

## Standard-Dateiformat ändern

Normalerweise sollte Word so eingestellt sein, dass Dokumente als Word-Datei gespeichert werden. Falls dies nicht der Fall ist, kann dies in den Einstellungen angepasst werden.

1. Menü __Dateien__ wählen.
2. Ganz unten __Optionen__ anklicken (Falls dieser Eintrag nicht sichtbar ist, zuerst __Mehr...__ anklicken).
3. In den _Word-Optionen_ den Bereich __Speichern__ auswählen.

![](./word-options-save.svg)

4. Bei _Dateien in diesem Format speichern_ das Format __Word-Dokument__ auswählen.

## Alte Word-Dokumente konvertieren

Wenn ein Dokument in einem veralteten Dateiformat geöffnet ist, steht in der Titelleiste von Word _Kompatibilitätsmodus_. Um ein solches Dokument in das aktuelle Word-Format zu konvertieren. wird so vorgegangen:

1. Menü __Dateien__ wählen.
2. Menüpunkt __Informationen__ anklicken.
3. Im Kasten _Kompatibilitätsmodus_ auf den Knopf __Konvertieren__ klicken:

![](./word-information-convert-1.png)

4. Im folgenden Fenster das Konvertieren mit __OK__ bestätigen:

![](./word-information-convert-2.png)

