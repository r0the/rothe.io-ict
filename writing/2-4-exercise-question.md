# :exercise: Aufgabe Frage
---

::: exercise Aufgabe Frage

1. Öffne Word und melde dich dort an der Schul-Cloud an.
2. Erstelle ein neues Dokument.
3. Schreibe eine Frage auf, die du im Informatikunterricht gerne beantwortet haben möchtest.
4. Speichere das Dokument auf deinem Schul-OneDrive.
5. Exportiere ein PDF-Dokument der Word-Datei.
6. Schicke das PDF-Dokument per Schul-E-Mail an _stefan.rothe@gymkirchenfeld.ch_
:::
