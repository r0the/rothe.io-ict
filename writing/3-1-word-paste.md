# Text einfügen
---

::: exercise Aufgabe Turing 1

Füge den folgenden Text in das Dokument **Turing** ein:

> # Alan Turing
> ## Kriegsheld und Mitbegründer der Informatik
> Der britische Mathematiker und Alan Turing lebte von 1912 bis 1954. Er entwickelte wichtige theoretische Grundlagen für die heutige Wissenschaft der Informatik.
> ### Studienzeit
> Während seiner Studienzeit am King’s College in Cambridge entwickelte er als Gedankenexperiment eine Maschine (heute «Turingmaschine») und bewies, dass diese alle lösbaren mathematischen Probleme auch lösen kann.
> ### Zweiter Weltkrieg
> Im Zweiten Weltkrieg war Turing im englischen Bletchley Park wesentlich daran beteiligt, mit der Enigma verschlüsselte deutsche Funksprüche zu knacken und so den Weltkrieg entscheidend zu beeinflussen.
> ### Nachkriegsjahre
> Ab 1945 arbeitete Turing an Konzepten für elektronische Computer. Ausserdem veröffentlichte er Arbeiten zur künstlichen Intelligenz und zu mathematischen Problemen der Biologie.
> Turing wurde 1952 wegen homosexueller Handlungen zu einer chemischen Therapie verurteilt und starb zwei Jahre später durch Suizid. 2013 wurde er durch Königin Elisabeth II offiziell begnadigt.

:::
