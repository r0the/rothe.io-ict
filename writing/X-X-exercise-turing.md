# :exercise: Aufgabe Turing
---

::: box warning
#### :warning: WICHTIG
Das Ziel dieser Übung ist es, dich mit der grundlegenden Bedienung von Word vertraut zu machen, wie sie bei uns an der Schule erwartet wird.

Wenn du bei einem Punkt nicht weisst, wie es geht, lese in den [Word-Anleitungen][1] nach. Wenn du damit nicht weiterkommst, frage den Lehrer.

**Verboten ist:**
- Ändern der Schriftart
- Ändern der Schriftgrösse
:::

::: box exercise
#### :exercise: Aufgabe Turing – Start

1. Öffne die Schul-Cloud im Browser. Klicke auf _Microsoft Teams_.

[![](../gk/images/cloud-link-teams.png)](https://teams.microsoft.com)

2. Gehe ins Team _IN Klasse_, wähle _Allgemein_, gehe zu _Dateien_, wähle _Kursmaterialien_.
3. Klicke beim Dokument _Alan Turing Vorlage.docx_ auf die drei Punkte und wähle den Menüpunkt _In Desktop-App öffnen_.

![](images/teams-turing-template.png)

4. Speichere die Datei in deinem eigenen Schul-OneDrive mit _Speichern unter_.
:::



::: box exercise
#### :exercise: Aufgabe Turing – Fussnoten

8. Erstelle aus den beiden roten Textabschnitten je eine Fussnote.
:::


::: box exercise
#### :exercise: Aufgabe Turing – Bild

9. Füge das folgende Bild in das Dokument ein:

![](/content/images/people/alan-turing.jpg "Alan Turing")

Das Bild soll einen quadratischen Textumbruch erhalten. Füge eine Beschriftung für das Bild ein.
:::

::: box exercise
#### :exercise: Aufgabe Turing – Verzeichnisse

10. Füge nach dem Untertitel ein Inhaltsverzeichnis ein.
***
Zur Kontrolle kannst du dein Dokument nun mit der Musterlösung vergleichen:

* [:pdf: Alan Turing Musterlösung](examples/alan-turing.pdf)
:::

[1]: ?page=2-0-word
