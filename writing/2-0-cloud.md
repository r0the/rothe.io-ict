# Cloud
---

«Cloud» bedeutet, dass deine Dateien auf einem Server des Cloud-Betreibers gespeichert werden. Üblicherweise steht auf dem Server ebenfalls eine Web-App zu Verfügung, damit die Dateien im Browser betrachtet oder bearbeitet werden können.

## Desktop-App

Damit die Desktop-App verwendet werden kann, muss diese erst auf dem Gerät installiert worden sein. Desktop-Apps bieten meistens einen grösseren Funktionsumfang.

## Web-App

Die Web-App wird beim Öffnen des Dokuments vom Server des Anbieters heruntergeladen und im Browser ausgeführt. Web-Apps haben meist einen eingeschränkten Funktionsumfang.

![](images/cloud-app.svg)


## Microsoft 365

### OneDrive im Browser

In **OneDrive im Browser** kann eine Datei wahlweise in der Web-App oder in der Desktop-App geöffnet werden. Mit einem Doppelklick wird die Datei in der Web-App geöffnet. Klicke auf die drei vertikalen Punkte :mdi-dots-vertical:, dann auf __Öffnen__ und __In der App öffnen__, um die Datei in der Desktop-App zu öffnen.

![](images/onedrive-open.png)

### Teams-App

In der **Teams-App** muss auf die drei horizontalen Punkte :mdi-dots-horizontal: geklickt werden. Es gibt zusätzlich die Möglichkeit __In Teams bearbeiten__. In diesem Fall wird die Web-App verwendet, diese wird jedoch innerhalb der Team-App gestartet (eigentlich ist die Teams-App auch ein Browser). 

![](images/teams-open.png)
