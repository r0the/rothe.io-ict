# PDF Exportieren
---

Um ein Word-Dokument als PDF-Datei zu exportieren, gehst du folgendermassen vor:

1. Wähle den Menüpunkt _Datei ‣ Exportieren_
2. Stelle sicher, dass _PDF/XPS-Dokument erstellen_ ausgewählt ist.
2. Klicke auf _PDF/XPS-Dokument erstellen_.

![](images/word-export-pdf-1.svg)

Nun erscheint das folgende Fenster. Hier klickst du einfach auf _Veröffentlichen_. Damit wird die PDF-Datei im gleichen Ordner wie die Word-Datei gespeichert.

![](images/word-export-pdf-2.png)
