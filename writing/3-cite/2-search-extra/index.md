# :extra: Zusatzinformationen
---

::: warning
Bitte schaue die Videos **nur mit Kopfhörern**.
:::
## SEO

Suchmaschinenoptimierung, auf englisch: Search Engine Optimization

<v-video id="jmdZ8rpRBDk"/>

## Filterblase

### Hintergrund

Suchmaschinen, soziale Netzwerke, Online-Shops, etc. speichern möglichst viele Daten über ihre Benutzer. Die Webseiten-Betreiber nutzen diese Daten, um die Inhalte speziell für jeden Benutzer auszuwählen. Inhalte, die dem Benutzer nicht gefallen oder nicht entsprechen, werden möglichst komplett ausgeblendet resp. unterbunden. Dadurch wird dem Benutzer eine verzerrte Sicht präsentiert:

- Es entsteht der Eindruck, dass alle so denken wie man selber.
- Fremde Meinungen werden so kaum mehr wahrgenommen.
- Als Benutzer kann man folglich nicht mehr finden, was im Internet vorhanden ist, sondern nur noch, was die Algorithmen der Anbieter für einen ausgewählt haben.

### Empfohlene Massnahmen

- «Private Browsing»-Modus zum Surfen verwenden, so werden verräterische Cookies stets automatisch gelöscht
- Suchmaschinen verwenden, die die Suchresultate nicht personalisieren (z.B. [DuckDuckGo](https://duckduckgo.com/) oder [SwissCows](https://swisscows.ch/))

### Weitere Informationen

**TED-Talk von Eli Pariser zum Thema Filterblase**

<v-video source="ted" id="eli_pariser_beware_online_filter_bubbles"/>

**Wie eindeutig wiedererkennbar bin ich im Internet unterwegs?**
https://panopticlick.eff.org/

## Lustiges mit Google

**Rechnen**
- Taschenrechnerfunktion in der Suchzeile integriert: [sqrt(3\*3+4\*4)](https://www.google.ch/search?q=sqrt(3*3%2B4*4))
- Umrechnen von Währungen, Masseinheiten, usw.
    - [10 chf in euro](https://www.google.ch/search?q=10%20chf%20in%20euro)
    - [10 imperial cup in liter](https://www.google.ch/search?q=5%20imperial%20cup%20in%20liter)

**Begriffe**
- Definitionen: [definiere Algorithmus](https://www.google.ch/search?q=definiere%20Algorithmus)
- Übersetzen: [ich habe hunger französisch](https://www.google.ch/search?q=ich%20habe%20hunger%20französisch)

**Reisen**
- Aktuelle Uhrzeit an einem anderen Ort: [Zeit in New York](https://www.google.ch/search?q=Zeit%20in%20New%20York)
- Fluginformationen abfragen: [SWISS  LX 16](https://www.google.ch/search?q=SWISS%20LX%2016)
- Wetterinformationen: [Wetter in New York](https://www.google.ch/search?q=Wetter%20in%20New%20York)

**Google Easter Eggs**
- z.B. ["do a barrel roll"](https://www.google.ch/search?q=do+a+barrel+roll)
- [mehr](https://searchengineland.com/the-big-list-of-google-easter-eggs-153768)

**Bildersuche**
- [images.google.ch](https://images.google.ch/)
- am besten kombinieren mit der erweiterten Suche
- Suchen nach ähnlichen Bildern

**Google-Suche einmal anders**
- [elgoog.im](https://elgoog.im/)
- [Hacker](https://www.google.com/?hl=xx-hacker)
