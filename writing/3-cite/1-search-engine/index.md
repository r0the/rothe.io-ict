# 1.2 Suchmaschinen
---

Suchmaschinen werden verwendet, um im World Wide Web nach Informationen zu suchen. Die bekannteste Suchmaschine ist Google mit einem Marktanteil von über 90%.

::: warning Einschränkungen
Suchmaschinen…
- können nicht alles finden -> [Deep Web](https://www.google.ch/search?q=Deep+Web)
- können beeinflusst werden -> [Search Engine Optimization](https://www.google.ch/search?q=Search+Engine+Optimization)
- beeinlussen die Suchergebnisse -> [Filterblase](?page=5-2-search-ex)
:::

## Funktionsweise

Eine Suchmaschine hat drei Hauptaufgaben:

- Erstellen eines Index
- Verarbeiten von Suchanfragen
- Bewerten und Filtern der Suchergebnisse

Bevor eine Suchmaschine eine Abfrage beantworten kann, muss erst eine Datenbank mit entsprechenden Informationen zu Webseiten erstellt werden. Diese Aufgabe wird von automatischen Programmen, sogenannten *Webcrawler*, übernommen, welche ständig das World Wide Web nach neuen und geänderten Seiten durchsuchen. Die Seiten werden analysiert und in einer riesigen Datenbank eingetragen. Dabei wird jeder Eintrag mit Stichworten versehen, welche sich aus der Analyse ergeben.

Wenn ein Benutzer eine Suchanfrage an die Maschine schickt, wird die Anfrage erst sprachlich analysiert. Beispielsweise werden falsch geschrieben Worte korrigiert. Anschliessend wird in der Datenbank nach Einträgen gesucht, deren Stichworte möglichst gut mit der Anfrage übereinstimmen.

Schliesslich werden die Resultate bewertet und entsprechend geordnet.

## Achtung

- Suchergebnisse werden von einem automatischen Lernsystem (künstliche Intelligenz) beeinflusst.
- Sie sind abhängig von der Person, vom Ort, vom Gerät und Browser.


## Suchmaschinen

::: cards 3
[![](./logo-google.svg)](https://www.google.ch/)
***
[![](./logo-startpage.svg)](https://www.startpage.com/)
***
[![](./logo-duckduckgo.svg)](https://duckduckgo.com/)
***
[![](./logo-swisscows.png)](https://swisscows.ch/)
***
[![](./logo-metager.png)](https://metager.de/)
***
[![](./logo-ecosia.png)](https://www.ecosia.org/)
:::
