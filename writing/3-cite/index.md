# Quellen verwenden
---

* [3.1 - Suchmaschinen](?page=1-search-engine/)
* [3.2 - Suche extra](?page=2-search-extra/)
* [3.3 - Quellenbewertung](?page=3-evaluate/)
* [3.4 - Quellenangaben](?page=4-citation/)
