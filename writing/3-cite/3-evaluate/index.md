# Quellenbewertung
---

::: warning
Hier geht es um die Bewertung von **allgemeinen Informationsquellen im Web**. Für **wissenschaftliche Quellen** gelten andere Kriterien.
:::

Im Web können viele widersprüchliche Informationen gefunden werden. Um zu beurteilen, ob wir einer Information vertrauen können, bewerten wir die Quelle anhand dieser Kriterien:

::: cards 2
#### Anbieter / Autorin
- Wer betreibt die Webseite (Regierung, Institution, Firma, Privatperson)?
- Wer hat die Information verfasst (Name, Beruf, Erfahrung)?
- Welche Motivation steckt dahinter?
- Wie wird das Angebot finanziert?

***

#### Aktualität
- Ist eine Datumsangabe vorhanden?
- Wann wurde die Seite publiziert/geändert?
- Ist die Information noch aktuell?

***

#### Absicht
- An wen richtet sich die Seite?
- Sind Ziel und Zweck erkennbar?
- Verständliche, angemessene Sprache?
- Professionelle, sorgfältige Darstellung?
- Wird Werbung eingesetzt?

***

#### Quellen
- Sind Originalquellen aufgeführt?
- Sind Quellen überprüfbar (z.B. Links)?
:::


::: info

### Beispiel

* [:pdf: Musterlösung.pdf](./assets/search_musterloesung.pdf)

:::
