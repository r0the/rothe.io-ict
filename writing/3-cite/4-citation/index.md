# Quellenangaben
---

> «Ein Buch zitieren, aus dem man einen Satz übernommen hat, heißt Schulden zahlen.»[^1]

Wird eine Quelle in einem eigenen Text verwendet, so muss eine **Quellenangabe** gemacht werden, um

- die ursprünglichen Autorinnen und Autoren zu **würdigen** und
- die im eigenen Text aufgestellten Behauptungen zu **belegen**.

Dazu muss die Quellenangabe so verfasst werden, dass der Leser, die Leserin die Quelle einfach **finden** und somit überprüfen kann.

Eine wissenschaftliche Quellenangabe für einen verwendeten Quelltext nennt man auch *Literaturangabe*.

Im folgenden werden vereinfachte Regeln für drei wichtige Fälle betrachtet.

## Literaturangabe für Bücher

In der Literaturangabe stehen in dieser Reihenfolge:

- Autor
- Titel
- Jahr der Veröffentlichung
- Publikationsort
- Verlag

**Beispiel:** Das Zitat am Anfang dieser Seite stammt von Umberto Eco aus seinem Werk «Wie man eine wissenschaftliche Abschlussarbeit schreibt», welches 2010 im UTB-Verlag veröffentlicht worden ist. Die entsprechende Literaturangabe sieht so aus:

::: example
Eco, Umberto (2020). *Wie man eine wissenschaftliche Abschlussarbeit schreibt*. UTB
:::

## Literaturangabe für Online-Quellen

Bei Online-Quellen müssen folgende Angaben gemacht werden:

- Autor oder Institution
- Titel
- Link (URL)
- Datum des Abrufs

**Beispiel:** Eine Literaturangabe für den Wikipedia-Artikel über Alan Turing sieht so aus:

::: example
Wikipedia: *Alan Turing*. [https://de.wikipedia.org/wiki/Alan_Turing][1] (abgerufen am 16.8.2019)
:::

**Beispiel:** Eine Literaturangabe für den oben erwähnten Zitierleitfaden der Technischen Universität München sieht so aus:

::: example
Technische Universität München, Universitätsbibliothek (2019). *TUM-Zitierleitfaden*. [https://mediatum.ub.tum.de/node?id=1231945][1] (abgerufen am 17.02.2019)
:::

## Quellenangabe für Bilder

Bei Bildern sind folgende Angaben notwendig:

- Autor
- Name des Webauftritts
- Link (URL)
- Datum des Abrufs

**Beispiel:** Für ein Bild der Website Pixabay lautet die Quellenangabe:

::: example
von Pexels, Pixabay, [https://pixabay.com/photo-1836990/][3] (abgerufen am 17.02.2019)
:::

[^1]: Umberto Eco: *Wie man eine wissenschaftliche Abschlussarbeit schreibt*, 2010, UTB

[1]: https://mediatum.ub.tum.de/node?id=1231945
[2]: https://intern.gymkirchenfeld.ch/school/showDocument?id=60004
[3]: https://pixabay.com/photo-1836990/
[4]: https://de.wikipedia.org/wiki/Alan_Turing
