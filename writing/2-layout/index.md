# 2 Layout
---

Unter Layout versteht man das Anordnen der Elemente eines Dokuments auf einer Seite.

![Seitenlayout](./layout.svg)

## Seitenstruktur
Eine Seite besteht aus einer Kopf- und einer Fusszeile sowie dem Hauptbereich. Zwischen Hauptbereich und Fusszeile werden allfällige Fussnotentexte angezeigt.

### Kopf- und Fusszeile
In der Kopfzeile werden üblicherweise der Autor/die Autorin und der Name des Werks dargestellt. In der Fusszeile wird die Seitennummerierung platziert.

### Hauptbereich
Im Hauptbereich der Seite wird der eigentliche Text des Dokuments platziert. Ausserdem werden hier Abbildungen und Tabellen eingefügt.

### Fussnoten
Fussnoten sind Anmerkungen, welche aus dem Fliesstext ausgelagert werden und auf einer Seite oberhalb der Fusszeile platziert werden[^1]. Im Fliesstext wird durch eine hochgestellte Zahl auf die entsprechende Fussnote verwiesen.

Textverarbeitungsprogramme stellen spezielle Funktionen zu Verfügung, um Fussnoten einzufügen. Fussnoten befinden sich **nicht** in der Fusszeile.


## Gestaltung der Elemente

### Absatz

Beim Layout werden Absätze durch einen Einzug oder einen Abstand nach der letzten Zeile optisch hervorgehoben.

[^1]: Wikipedia: Fussnote, [https://de.wikipedia.org/wiki/Fu%C3%9Fnote][1] (abgerufen am 24.02.2019)

[1]: https://de.wikipedia.org/wiki/Fu%C3%9Fnote
