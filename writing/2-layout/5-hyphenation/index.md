# 2.5 Worttrennung
---

Worttrennung ist das Auftrennen von Wörtern mit Bindestrichen, um den Fliesstext kompakter darstellen zu können. Dies spart Platz und ist gerade bei Druckwerken auch eine ökonomische Frage.

Ausserdem wirkt das Schriftbild gerade bei mehrspaltigen Texten mit Worttrennung regelmässiger und somit ästhetischer:

![Text ohne und mit Worttrennung](./hyphenation.svg)

## Worttrennung in Word aktivieren

Öffne das Menü _Layout_ und klicke auf _Silbentrennung_. Wähle den Eintrag __Automatisch__, um die Worttrennung in Word zu aktivieren.

![](./word-hyphenation.png)

Wähle den Eintrag __Keine__ um die Worttrennung wieder zu deaktivieren.
