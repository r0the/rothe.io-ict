# 2.3 Abbildungen
---

::: info
#### Kurzanleitung
Beim Einfügen eines Bildes in Word solltest du immer nach folgendem Schema vorgehen:
1. Bild einfügen
2. Textumbruch auf «Quadrat» (macOS: «Rechteck») ändern
3. Grösse anpassen
4. Beschriftung einfügen
5. Bild und Beschriftung gruppieren
:::

## 1. Einfügen

In Microsoft Word wird ein Bild über den Menüpunkt _Einfügen ‣ Bilder_ eingefügt.

![Bild einfügen in Microsoft Word](./word-image-1.png)

## Textfluss

Wenn ein Bild in Word einfügt, so hat es zunächst den Textfluss «Mit Text in Zeile». Dabei verhält sich das Bild wie ein einzelner Buchstabe im Text, also wie ein sehr grosser Emoji.
::: columns 2
![Textfluss «Mit Text in Zeile»](./wrap-text-1.png)
***
![Textfluss «Quadrat» bzw. «Rechteck»](./wrap-text-2.png)
:::

Normalerweise wollen wir aber ein Bild beliebig auf der Seite platzieren können. Dazu muss die Textfluss-Eigenschaft des Bildes auf «Quadrat» (macOS: «Rechteck») umgeschaltet werden. Nun «fliesst» der Text um das Bild herum.

::: warning
**Achtung:** Der Textfluss muss unbedingt umgestellt werden, ansonsten kann das Bild später auch nicht mit der Beschriftung gruppiert werden.
:::

## Grösse ändern

Die Grösse des Bildes kann angepasst werden, indem an einer der Ecken gezogen wird. Drücke die [Shift]-Taste während des Ziehens, um das Seitenverhältnis zu bewahren und das Bild nicht zu verzerren.

## Beschriftung einfügen

Um eine Beschriftung für ein Bild einzufügen, muss erst das Bild durch anklicken ausgewählt werden. Anschliessend wird der Menüpunkt _Referenzen ‣ Beschriftung einfügen_ ausgewählt.

![Bildbeschriftung einfügen 1](./word-image-caption-1.png)

Word zeigt ein Dialogfenster an, in welchem die Beschriftung eingegeben werden kann. Mit einem Klick auf _OK_ wird die Beschriftung in das Dokument eingefügt.

![Bildbeschriftung einfügen 2](./word-image-caption-2.png)

## Bild und Beschriftung gruppieren

Normalerweise möchte man ein Bild zusammen mit seiner Beschriftung verschieben. Dies geht einfacher, wenn die beiden Elemente gruppiert werden. Das geht so:

- Klicke auf das Bild, um es auszuwählen.
- Drücke [Shift] und klicke auf die Beschriftung, um diese zusätzlich auszuwählen.
- Öffne das Kontextmenü (Rechtsklick oder [Menu]-Taste), und wähle den Menüpunkt _Gruppieren ‣ Gruppieren_.

![Bild und Bildbeschriftung gruppieren](./gruppieren.gif)
