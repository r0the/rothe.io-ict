# 2.2 Kopf- und Fusszeile
---

## Kopf- und Fusszeile bearbeiten

Mit einem _Doppelklick_ auf den oberen bzw. untern Rand der Seite kann die Kopf- bzw. Fusszeile bearbeitet werden.

![Kopfzeile in Microsoft Word](./word-header.png)

![Fusszeile in Microsoft Word](./word-footer.png)

Word zeigt durch eine graue Box mit dem Inhalt _Kopfzeile_ bzw. _Fusszeile_ an, dass diese aktiviert ist.

Durch einen _Doppelklick_ auf den Mittelteil der Seite wird das Bearbeiten der Kopf- und Fusszeile beendet.

## Tabulatoren

Die Kopf- und Fusszeile ist durch Tabulatoren in einen **linken**, einen **mittleren** und einen **rechten** Teil unterteilt. Die Inhalte in diesen Teilen werden entsprechend linksbündig, zentriert und rechtsbündig ausgerichtet.

Die Teile werden durch ein Tabulatorzeichen voneinander getrennt. Dieses unsichtbare Formatierungszeichen wird mit der Tabulatortaste [Tab] eingefügt.

Zur besseren Übersicht können in Word [Formatierungszeichen eingeblendet werden](?page=../../1-outline/7-formatting-marks/).
