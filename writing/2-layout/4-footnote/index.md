# 2.4 Fussnoten
---

In Microsoft Word können Fussnoten über den Menüpunkt _Referenzen ‣ Fußnote einfügen_ eingefügt werden.


![Fussnote einfügen in Microsoft Word](./word-footnote.png)
