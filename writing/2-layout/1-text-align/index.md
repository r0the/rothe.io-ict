# 2.1 Textausrichtung
---

Text kann auf vier unterschiedliche Arten ausgerichtet werden:

::: columns 2
![linksbündig](./text-align-left.svg)
***
![rechtsbündig](./text-align-right.svg)
***
![zentriert](./text-align-center.svg)
***
![Blocksatz](./text-align-block.svg)
:::

In Word ist eine **linksbündige** Ausrichtung voreingestellt. Zur besseren Leserlichkeit kann es von Vorteil sein, **Blocksatz** zu verwenden.

Eine **zentrierte** Ausrichtung wird beispielsweise für Titel oder für Lyrik verwendet.

Die **rechtsbündige** Ausrichtung kann für spezielle Gestaltungswüsche ebenfalls Sinn machen.

Die Ausrichtung kann pro Absatz festgelegt werden. Dazu sollte die [Formatvorlage angepasst werden](?page=../../1-outline/5-style-change/), welche für den entsprechenden Absatz verwendet wurde.
