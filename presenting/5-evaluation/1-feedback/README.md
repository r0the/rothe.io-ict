# Rückmeldung Präsentation
---

* [:download: Arbeitsblatt (PDF)](./presentation-feedback.pdf)

## Gestaltung der Folien

- Design
- Schrift
- Text
- Animationen

## Inhalt der Präsentation

- Einstieg
- Zusammenhang
- Bezug zu Folien
- Hauptaussage

## Person

- Haltung
- Hilfsmittel (Zettel, Presenter)
- Kontakt zu Publikum
- Stimme
