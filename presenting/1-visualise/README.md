# Visualisieren
---

## Motivation

> «One Look is Worth A Thousand Words.»
>
> *[Fred R. Barnard, Werbeanzeige in Printers’ Ink, 1921][1]*

Heute weiss man aus der psychologischen Forschung, dass die Behaltensquote, also die Wahrscheinlichkeit, dass man eine aufgenommene Information nicht wieder vergisst, zunimmt, wenn die Information über mehrere Kanäle aufgenommen wird.[^1]

![Behaltensquote für verschiedene Arten der Informationsaufnahme](./learning-quota.svg)

## Gestaltungselemente

Eine Visualisierung wird normalerweise aus verschiedenen Gestaltungselementen zusammengestellt. Wir unterschieden folgende grundlegende Elemente:

::: cards 2
#### Text

**Behaltensquote**

hören: 20%, sehen 30%, hören und sehen: 50%

***
#### Grafik und Symbole

![](./see-hear.svg)
***
#### Diagramme

![](./learning-quota.svg)

***
#### Listen und Tabellen

| Informationsaufnahme    | Behaltensquote |
|:----------------------- | --------------:|
| hören                   |            20% |
| sehen                   |            30% |
| hören und sehen         |            50% |
| nacherzählen / erklären |            70% |
| selbst machen           |            90% |

:::

[^1]: Kevin Schroer: Die Psychologie des Lernens – Wie funktioniert Lernen?,
[fernstudieren.de](https://www.fernstudieren.de/im-studium/effektives-lernen/die-psychologie-des-lernens/), abgerufen am 25.02.2019

[1]: https://de.wikipedia.org/wiki/Ein_Bild_sagt_mehr_als_tausend_Worte
