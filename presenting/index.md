# Präsentieren
---


Eine Präsentation ist ein Vortrag mit **unterstützender Visualisierung**. Andere Vortragsformen wie die politische Rede verzichten auf eine visuelle Darstellung.

::: columns 2
![Steve Jobs an der WWDC 2008 ©](./steve-jobs-wwdc-2008.jpg)
***
![Angela Merkel im Bundestag 2014 ©](./bundestag.jpg)
:::

Die wichtigsten Erfolgsfaktoren für eine Präsentation sind

- ein ausgefeilter Aufbau,
- eine gelungene Visualisierung und
- gekonntes Präsentationsverhalten.[^1]

#### Links

* [:presentation: Präsentation](?slides=s-present)
* [:youtube: Apple World Wide Developer Conference 2018](https://youtu.be/UThGcWBIMpU?t=260)

[^1]: Josef W. Seifert: Visualisieren, Präsentieren, Moderieren; 40. Auflage, Gabal, 2018
