# Durchführung
---

## Wissen vermitteln – So hältst du einen guten Vortrag

Ein Vortrag von Mai vom YouTube-Kanal [maiLab](https://www.youtube.com/channel/UCyHDQ5C6z1NDmJ4g6SerW8g).

<v-video id="wkCrFctGcEM">
(18:02 Minuten)
</v-video>

## So präsentiert ein Profi

<v-video id="8O7CusRyWC4">
(19:31 Minuten)
</v-video>


## Fünf Tipps gegen Lampenfieber

<v-video id="IVVrG8Rrq7c">
(5:26 Minuten)
</v-video>

## Präsentieren wie ein Profi - Tipps für Ihren überzeugenden Auftritt

<v-video id="aFkKnSwMDsk">
(6:12 Minuten)
</v-video>
