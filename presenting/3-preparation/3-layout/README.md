# Folienlayout

Eine neue Präsentation startet normalerweise mit einer Titelfolie. Weitere Folien kannst du über _Start ‣ Neue Folie_ eingefügen.

![Folie einfügen und Layout wählen](./folie-einfuegen.png)

Dabei wählst du das gewünschte Folienlayout aus.

Beim **Folienlayout** handelt es sich um eine vordefinierte Anordnung von Elementen auf der Folie. Dazu gehören z.B. Bereiche für Überschrift, Text und Bilder. Durch die Verwendung der Folienlayouts stellst du sicher, dass Elemente auf allen Folien korrekt und konsistent platziert sind. Folienelemente manuell anpassen solltest du nur in ganz speziellen Situationen.

Das Layout einer Folie kannst du auch nachträglich anpassen. Dies klappt entweder mit Rechtsklick auf das Vorschaubild der entsprechenden Folie oder über _Start ‣ Layout_.

![Layout einer Folie ändern](./folienlayout.png)

Beim Ändern des Layouts können gewisse Inhalte verloren gehen, wenn das neue Layout Bereiche des alten Layouts nicht unterstützt,
