# Vorbereitung
---

Bei der Vorbereitung einer Präsentation sollte man zuerst folgende Fragen beantworten:

1. Was will ich mitteilen?
2. Wer ist das Zielpublikum?
3. Wo werde ich die Präsentation halten?

Die Antworten auf diese Fragen dienen als Grundlage für die weiteren Vorbereitungsschritte.
