# Präsentationsmittel
---

## Analog und Digital

::: cards 3
![©](./steve-jobs-wwdc-2008.jpg)
#### Videoprojektor

***
![©](./flip-chart.jpg)
#### Flipchart

***
![©](./blackboard.jpg)
#### Wandtafel
:::

## Präsentationssoftware

| Programm                      | Kosten               | Plattform             |
|:----------------------------- |:-------------------- |:--------------------- |
| [LibreOffice Impress][1]      | gratis (Open Source) | Windows, macOS, Linux |
| [FreeOffice Presentations][2] | gratis               | Windows, macOS, Linux |
| [Google Slides][3]            | gratis               | Web                   |
| [Microsoft PowerPoint][4]     | ca. 90.- / Jahr      | Windows, macOS        |
| [Apple Keynote][5]            | gratis               | macOS, Web            |
| [Prezi][6]                    | gratis / Miete       | Web                   |


[1]: https://de.libreoffice.org/
[2]: https://www.freeoffice.com/de/
[3]: https://docs.google.com/
[4]: https://products.office.com/
[5]: https://www.apple.com/de/iwork/
[6]: https://prezi.com/
