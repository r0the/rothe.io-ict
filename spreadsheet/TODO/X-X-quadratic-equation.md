# Quadratische Gleichung
---

## Ziel

In der Arbeitsmappe «quadratische-gleichung» sollen automatisch die Lösungen einer quadratischen Gleichung berechnet werden.

* [:file-excel: qudratische-gleichung.xlsx](exercise/quadratische-gleichung.xlsx)

Wenn jemand in den Zellen B5, D5 und F5 Werte eintippt, sollen in den Zellen J6 und J7 automatisch die Lösungen angezeigt werden.

::: box exercise
#### :exercise: Aufgabe: Quadratische Gleichung lösen

- Benenne die weissen Zellen.
- Schreibe in die Zelle J5 eine Formel, welche die Diskriminante berechnet.
- Schreibe in die Zellen J7 und J8 die Formeln welche die Lösungen der Gleichung berechnen.

**Achtung:** 'c' kann nicht als Name einer Zelle verwendet werden.
:::

::: box exercise
#### :exercise: Aufgabe: Funktionsgraph zeichnen

- Fülle die Spalte «x» in der Wertetabelle mit den Werten -5, -4.9, -4.8, ... 5.
- Gib in der Spalte «y» die Formel für die Berechnung von $y=ax^2+bx+c$ ein.

Erstelle ein Diagramm, welches den Graphen der Funktion aufgrund der Wertetabelle darstellt. Das Diagramm soll so formatiert sein:

- horizontale Achse von -5 bis 5
- vertikale Achse von -20 bis 20
- keine Gittenetzlinien
- keine Achsentitel
:::

::: box exercise
#### :extra: Zusatzaufgabe: Sonderfälle behandeln

- Wenn keine Lösung gefunden werden kann, soll der Text «Keine Lösung» angezeigt werden.
- Passe die Formel in Zelle J8 so an, dass der Text «nur eine Lösung» ausgegeben wird, wenn die Diskriminante 0 ist.
:::
