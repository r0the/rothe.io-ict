# Notenrechner
---

## Ziel

Du sollst ein Tabellenblatt entwerfen, in welchem du Deine Noten eintragen kannst. Das Tabellenblatt soll automatisch die Zeugnisnoten berechnen sowie ausrechnen, ob das Zeugnis genügend ist.

Wann ein Zeugnis genügend ist, ist in der [Mittelschuldirektionsverordnung (MiSDV)](https://www.belex.sites.be.ch/frontend/versions/1304) des Kantons Bern festgelegt:

> Art. 50
>
> 1 Die Zeugnisnoten der folgenden Fächer legen fest, ob die Gesamtleistung in einer Beurteilungsperiode genügend oder ungenügend ist:
>
> - Erstsprache,
> - zweite Landessprache,
> - dritte Sprache (Englisch oder Italienisch oder Latein),
> - Mathematik,
> - Biologie,
> - Chemie,
> - Physik,
> - Geschichte,
> - Geographie,
> - Einführung in Wirtschaft und Recht,
> - Bildnerisches Gestalten oder Musik,
> - Schwerpunktfach,
> - Ergänzungsfach.
>
> 2 Die Gesamtleistung ist genügend, wenn von den Noten gemäss Absatz 1
>
> - die doppelte Summe aller Notenabweichungen von 4 nach unten nicht grösser ist als die Summe aller Notenabweichungen von 4 nach oben und
> - nicht mehr als vier Noten unter 4 erteilt werden.
>
> 3 Im Übrigen richtet sich die Promotion nach Artikel 18.

Keine Angst, falls du das nicht verstehst, weiter unten wird diese Definition genauer erklärt.

::: box exercise
#### :extra: Aufgabe: Notentabelle

Im ersten Schritt sollst eine Notentabelle anlegen, welche so aufgebaut ist:

- In der ersten Spalte stehen die Fächer.
- Danach folgen fünf Spalten für die Einzelnoten.
- In der nächsten Spalte «Schnitt» wird der Durchschnitt (Mittelwert) der fünf Noten berechnet.
- In der letzten Spalte «Zeugnisnote» steht der auf eine halbe Note gerundete Durchschnitt.

| Fach        | 1. Note | 2. Note | 3. Note | 4. Note | 5. Note | Schnitt | Zeugnisnote |
| ----------- | -------:| -------:| -------:| -------:| -------:| -------:| -----------:|
| Deutsch     |       5 |     5.5 |      5  |         |         | 5.16667 |           5 |
| Französisch |         |         |         |         |         |         |             |
| ...         |         |         |         |         |         |         |             |

Die folgenden Fächer müssen in der Tabelle stehen:

- Deutsch (*Erstsprache*)
- Französisch (*zweite Landessprache*)
- Englisch, Italienisch oder Latein (*dritte Sprache*)
- Mathematik
- Biologie
- Chemie
- Physik
- Geschichte
- Geografie
- Bildnerisches Gestalten oder Musik (*Kunstfach*)
- Schwerpunktfach

In den Spalten «Schnitt» und «Zeugnisnote» musst du geeignete Formeln eingeben, welche die gewünschten Berechnungen durchführen.

Teste Deine Formeln, indem du bei allen Fächern drei bis fünf Noten eingibst.

**Achtung:** Dass die Zeugnisnote durch den Schnitt der Einzelnoten gebildet wird, ist eine vereinfachte Annahme. Den Lehrerinnen und Lehrern steht es grundsätzlich frei, wie sie zur Zeugnisnote gelangen.
:::

::: box exercise
#### :extra: Zusatzaufgabe: Promotionsregel

Nun soll berechnet werden, ob die Zeugnisnoten ein genügendes Zeugnis ergeben. Dazu muss erst die Abweichung der Noten von 4 berechnet werden. Anschliessend müssen die negativen sowie die positiven  Abweichungen separat zusammengezählt werden. Auch die Anzahl negativer Abweichungen muss gezählt werden. Dazu definieren wir vier zusätzliche Spalten:

- **Abweichung von 4**: Differenz von der Zeugnisnote und 4.
- **Summe kleiner 4**: Enthält die Abweichung, falls sie kleiner als 0 ist, sonst eine 0.
- **Summe grösser 4**: Enthält die Abweichung, falls sie grösser als 0 ist, sonst eine 0.
- **Anzahl kleiner 4**: Enthält eine 1, falls die Abweichung kleiner als 0 ist, sonst eine 0.

Für die letzten drei Spalten bilden wir die Summe. In der Zelle

- **J14** steht die Summe der Abweichungen von 4 nach unten,
- **K14** steht die Summe der Abweichungen von 4 nach oben,
- **L14** steht die Anzahl der Abweichungen von 4 nach unten.

Nun können die Bedingungen aus dem Artikel 50 der MiSDV so formuliert werden: Das Zeugnis ist genügend, falls:

- `-2 * J14 <= K14`
- `L14 <= 4`

Wir definieren in einem freien Bereich des Tabellenblatts eine kleine Tabelle, welche für jede Bedingung festhält, ob sie erfüllt ist. Falls die Bedingung erfüllt ist, soll in der Spalte «erfüllt?» eine 1 stehen, sonst eine 0.

| Bedingung                   | erfüllt? |
| --------------------------- | --------:|
| Summe der Notenabweichungen | 1        |
| Anzahl Noten kleiner 4      | 1        |
| Summe Bedingungen           | 2        |

Nun wird die Summe dieser Spalte gebildet. Alle Bedingungen sind erfüllt und das Zeugnis damit genügend, falls die Summe der Spalte 2 beträgt.
:::
