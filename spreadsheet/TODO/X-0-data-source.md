# Datenquellen
---



[1]: https://wikitable2csv.ggor.de/

bfs.admin.ch

https://www.mfk.ch/bigdata/

https://www.instacart.com/datasets/grocery-shopping-2017


## Datenquellen für Beispiele

* [:link: BFS: Nationalratswahlen: Mandatsverteilung nach Parteien][1]
* [:link: BFS: Nationalratswahlen: Mandatsverteilung nach Parteien und Geschlecht][2]
* [:link: BFS: Bevölkerungsdaten im Zeitvergleich, 1950-2018][3]



[1]: https://www.bfs.admin.ch/bfs/de/home/statistiken/politik/wahlen/nationalratswahlen.assetdetail.11048413.html
[2]: https://www.bfs.admin.ch/bfs/de/home/statistiken/politik/wahlen/frauen.assetdetail.11048432.html
[3]: https://www.bfs.admin.ch/bfs/de/home/statistiken/bevoelkerung.assetdetail.9466629.html
