# Achsen formatieren
---

## Achsentitel

Klicke auf das Plus-Symbol, um das Menü _Diagrammelemente_ anzuzeigen.
Klicke anschliessend auf den Pfeil _‣_ rechts vom Menüpunkt _Achsentitel_. Setze im nun erscheinenden Untermenü beim gewünschten Menüpunkt einen Haken.

- _Primär horizontal_: Achsentitel für die horizontale Achse
- _Primär vertikal_: Achsentitel für die vertikale Achse

![](images/excel-axis-title.png)

Klicke anschliessend auf den umrahmten Achsentitel um diesen abzuändern.

Durch das Ziehen am Rahmen des Titels kann dieser positioniert und in seiner Grösse angepasst werden.

## Achse formatieren

Klicke auf das Diagramm und öffne das Menüband _Diagrammtools ‣ Format_.

Im Menübereich _Aktuelle Auswahl_ wird das aktuell gewählte Diagrammelement angezeigt. Die Auswahl kann entweder durch einen Klick auf das gewünschte Element oder über das Dropdown-Menü geändert werden.

![](images/excel-diagram-selection.png)

Wähle einer der folgenden Diagrammelemente, um die entsprechende Achse zu formatieren:

- _Horizontal (Wert) Achse_
- _Vertikal (Wert) Achse_

Klicke anschliessend auf die Schaltfläche _Auswahl formatieren_, um am rechten Rand des Excel-Fensters den Bereich _Achse formatieren_ einzublenden:

![](images/excel-format-axis.png)

## Wichtige Achsenformatierungen

Mit **Minimum** und **Maximum** wird festgelegt, welcher Bereich der Achse angezeigt wird.

Das **Hauptintervall** legt fest, in welchem Abstand Werte auf der Achse angeschrieben werden. In diesem Abstand werden auch die Gitternetzlinien gezeichnet.

Bei grossen Werten bietet es sich an die **Anzeigeeinheiten** beispielsweise auf «Millionen» zu setzen. Die Achse wird dann automatisch entsprechend beschriftet, also z.B. mit «42» statt «42'000'000». Dabei sollte auch immer die Option _Beschriftung der Anzeigeeinheiten im Diagramm anzeigen_ eingeschaltet werden.
