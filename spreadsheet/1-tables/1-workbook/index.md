# 1.1 Arbeitsmappe
---

<!-- <v-video id="HJerEWmDIeI"/> -->

Die Dateien, welche mit einem Tabellenkalkulationsprogramm bearbeitet werden, heissen **Arbeitsmappen**. Eine Arbeitsmappe enthält eine oder mehrere **Tabellenblätter**. Eine Tabelle besteht aus **Zellen**, welche in **Zeilen** und **Spalten** angeordnet sind.

Die Zeilen werden mit Zahlen bezeichnet, die Spalten mit Buchstaben. Nach der Spalte **Z** folgt **AA**, dann **AB**, usw.

So erhält jede Zelle einer Tabelle eine eindeutige Bezeichnung, z.B. **A13** oder **ZA210**.

![](./excel-overview.svg)

## aktive Zelle

Im Tabellenkalkulationsprogramm ist immer eine Zelle ausgewählt. Sie wird als **aktuelle** oder **aktive** Zelle bezeichnet. Oberhalb der Tabelle wird die Bezeichnung sowie der Inhalt der aktuellen Zelle dargestellt.

## Tabellenblatt wechseln

In Tabellenkalkulationsprogrammen wird normalerweise nur eine Tabelle einer Arbeitsmappe angezeigt. Mit Tabs am unteren Rand der Tabelle kann zu den anderen Tabellen in der Arbeitsmappe gewechselt werden.

## Tabellenblätter verwalten

Mit einem **Rechtsklick** auf den Namen eines Tabellenblatts wird das folgende Menü aufgerufen:

![](./excel-sheets.png)

Hier kann das Tabellenblatt verwaltet werden:

- _Einfügen…_ erstellt ein neues Tabellenblatt.
- _Löschen_ löscht das angeklickte Tabellenblatt.
- _Umbenennen_ ermöglicht es, dem Tabellenblatt einen neuen Namen zu geben.

Ein neues Tabellenblatt kann auch mit einem Klick auf :mdi-plus-circle-outline: erstellt werden.
