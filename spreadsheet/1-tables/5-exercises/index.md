# :exercise: Aufgaben

::: exercise Vorbereitung
Laden Sie die untenstehende Excel-Datei herunter und speichern Sie sie auf Ihrem OneDrive.

Diese Datei dient als Vorlage für die restlichen Übungen in diesem Abschnitt.

* [:xlsx: Gebiete der Erde.xlsx](./gebiete-der-erde.xlsx)
:::

::: exercise Sortieren und Filtern
1. Lesen Sie die Seite «Sortieren und Filtern».

2. Erstellen Sie ein neues Tabellenblatt mit dem Namen «Aufgabe 2». Schreiben Sie die Antworten auf die folgenden Fragen in dieses Tabellenblatt.

3. Beantworten Sie die folgenden Fragen, indem Sie die Filter-Funktion sinnvoll verwenden:
    - Welches ist das Gebiet in Polynesien mit den meisten Einwohnern?
    - Wie viele unabhängige, anerkannte Staaten gibt es (UN-Mitglieder)?
    - Welches ist der unabhängige Staat mit den wenigsten Einwohnern?
    - Welches ist der unabhängige Staat mit der kleinsten Fläche?
    - Wie heisst das Gebiet mit dem Hauptort «Flying Fish Cove» und zu welchem Staat gehört - es?
    - Wie viele unabhängige, anerkannte Staaten gibt es in Europa?
    - Welche Staaten sind nicht vollständig international anerkannt (d.h. nicht UN-Mitglied und gehört nicht zu einem anderen Staat)

* [Sortieren und Filtern](?page=../2-filter/)
:::


::: exercise Auswählen und Kopieren
1. Lesen Sie die Seite «Auswählen und Kopieren».

2. Erstellen Sie ein neues Tabellenblatt mit dem Namen «Amerika».

3. Kopieren Sie alle Staaten und Gebiete, die in Amerika (inkl. Karibik) liegen, in das neue Tabellenblatt.

* [Auswählen und Kopieren](?page=../3-select-copy/)
:::

::: exercise Zahlenformate

1. Lesen Sie die Seite «Zahlenformate».

2. Formatieren Sie die Tabelle Staaten und Gebiete folgendermassen:
    - **Einwohner 2017:** Format: Zahl, Tausendertrennzeichen, keine Nachkommastellen
    - **Anteil Weltbevölkerung:** Prozentwert
    - **Fläche:** Format: Zahl, Tausendertrennzeichen, eine Nachkommastelle
    - **Einw. pro km2:** Format: Zahl, eine Nachkommastelle

* [Zahlenformate](?page=../4-number-format/)
:::
