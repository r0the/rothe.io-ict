# 1.2 Sortieren und Filtern
---

<!-- <v-video id="G9Mvx0m1-Zw"/> -->

## Filter ein-/ausschalten

Der Filter wird mit der Tastenkombination [Shift]+[Ctrl]+[L] oder mit dem Menüpunkt _Start ‣ Sortieren und Filtern ‣ Filtern_ ein- und wieder ausgeschaltet.

![](./excel-filter-1.svg)

## Sortieren und Filtern

Wenn der Filter in einer Tabelle eingeschaltet wurde, wird in jeder Spaltenüberschrift das Icon ![](./dropdown.png) eingeblendet. Damit kann ein Menü eingeblendet werden, welches das Sortieren und Filtern nach dieser Spalte ermöglicht:

![](./excel-filter-2.png)

## Filtern

Um nur die Zeilen anzuzeigen, welche bestimmte Werte enthalten, wird bei den entsprechenden Werten ein Haken gesetzt.

Durch Klicken auf _(Alles auswählen)_ können alle Einträge gewählt oder abgewählt werden. Durch eine Eingabe im Feld _Suchen_ kann nach bestimmten Einträgen gesucht werden.
