# 1.3 Auswählen und Kopieren
---

## Auswählen

<!-- <v-video id="FYoTA98c_bU"/> -->

### Bereich auswählen

Gehe so vor, um einen Bereich auszuwählen:

1. Klicke die obere linke Ecke des Bereichs an.
2. Halte die Maustaste gedrückt und ziehe den Mauszeiger zur unteren rechten Ecke.
3. Lasse die Maustaste los.

![](./excel-select-range.svg)

### Zeilen auswählen

Gehe so vor, um mehrere Zeilen auszuwählen:

1. Klicke auf die erste Zeilennummer, die du auswählen willst.
2. Halte die Maustaste gedrückt und ziehe den Mauszeiger zur letzten Zeilennummer.
3. Lasse die Maustaste los.

![](./excel-select-rows.svg)

### Spalten auswählen

Gehe so vor, um mehrere Spalten auszuwählen:

1. Klicke auf die erste Spaltennummer, die du auswählen willst.
2. Halte die Maustaste gedrückt und ziehe den Mauszeiger zur letzten Spaltennummer.
3. Lasse die Maustaste los.

![](./excel-select-columns.svg)

### bestimmte Zeilen auswählen

Gehe so vor, um mehrere, nicht aufeinanderfolgende Zeilen auszuwählen:

1. Klicke auf die erste Zeilennummer, die du auswählen willst und lasse die Maustaste wieder los.
2. Halte die [Ctrl]-Taste (macOS: [Command]-Taste) gedrückt und klicke alle weiteren Zeilennummern an, welche du auswählen willst.
![](./excel-select-multi.svg)

### Auswählen und Filter

In einer gefilterten Tabelle werden nur die **sichtbaren** Daten ausgewählt.

## Kopieren

Gehe so vor, um einen Tabellenbereich zu kopieren.

1. Wähle den gewünschten Tabellenbereich aus.
2. Drücke [Ctrl]+[C] (macOS: [Command]+[C]).
3. Wähle die oberste linke Zelle des Bereichs aus, wo du die Daten hinkopieren willst.
4. Drücke [Ctrl]+[V] (macOS: [Command]+[V]).
