# 1.4 Zahlenformate
---

Die Darstellung von Zahlenwerten in einer Zelle kann durch die Wahl eines **Zahlenformats** beeinflusst werden. In Excel wird dazu im Menü _Start_ der Abschnitt _Zahl_ verwendet:

![](./excel-format-ribbon.png)

Über das Dropdown-Menü können verschiedene vordefinierte Zahlenformate gewählt werden:

![](./excel-format-dropdown.png)

Das Format wird jeweils dem **ausgewählten Bereich** zugewiesen. Um also eine ganze Spalte gleich zu formatieren, wird zuerst die Spalte ausgewählt, anschliessend wird das gewünschte Format gewählt.

Im folgenden werden die wichtigsten Darstellungsarten kurz erläutert.

## Zahl

Wir im Dropdown-Menü der Eintrag _Zahlenformat_ ausgewählt, dann wird der Wert als «normale» Zahl dargestellt. Mit den folgenden drei Schaltflächen können die Anzahl Nachkommastellen eingestellt und die Darstellung eines Tausendertrennzeichens ein- und ausgeschaltet werden:

![](./excel-format-thousand-and-decimals.png)

## Prozentwert

Eine Zahl kann auch als Prozentwert dargestellt werden. Die kann mit der markierten Schaltfläche oder über den Eintrag _Prozent_ im Dropdown-Menü geschehen.

::: warning
**Achtung:** Hier ist zu beachten, dass der Prozentwert das hundertfache des Zahlwerts ist. Die Zahl 0.42 entspricht 42%.
:::

![](./excel-format-percent.png)

## Wissenschaftliche Notation

In der Wissenschaft werden sehr grosse oder sehr kleine Zahlen oft in der sogenannten **wissenschaftlichen Notation** dargestellt. Dabei wird eine grosse Anzahl Nullen durch eine Zehnerpotenz ersetzt. Anstelle von 1000000 schreibt man 10^6, anstelle von 2490000 schreibt man 2.49·10^6.

In Excel wird die wissenschaftliche Notation durch den Eintrag _Exponentialzahl_ im Dropdown-Menü ausgwählt.
