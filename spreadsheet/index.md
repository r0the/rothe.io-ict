# Daten auswerten
---

* [:goal: Lernziele](?page=0-goals/)
* [1 Tabellen verwenden](?page=1-tables/)
* [2 Diagramme](?page=2-charts/)
* [3 Formeln und Zellbezüge](?page=3-formulas/)
* [4 Datenbanken](?page=4-database/)
