# Weizenkornlegende
---

## Sissa ibn Dahir

Sissa ibn Dahir lebte angeblich im dritten oder vierten Jahrhundert in Indien und gilt Legenden zufolge als der Erfinder des Schachspiels.

![](./sissa.jpg)

## Weizenkornlegende

Der indische Herrscher Shihram tyrannisierte seine Untertanen und stürzte sein Land in Not und Elend. Um die Aufmerksamkeit des Königs auf seine Fehler zu lenken, ohne seinen Zorn zu entfachen, schuf der weise Brahmane Sissa ein Spiel, in welchem der König als wichtigste Figur ohne Hilfe anderer Figuren und Bauern nichts ausrichten kann. Der Unterricht im Schachspiel machte auf den Herrscher Shihram einen starken Eindruck. Er wurde milder und liess das Schachspiel verbreiten, damit alle davon Kenntnis nähmen.

Um sich für die anschauliche Lehre von Lebensweisheit und zugleich Unterhaltung zu bedanken, gewährte er dem Brahmanen einen freien Wunsch. Dieser wünschte sich Weizenkörner: Auf das erste Feld eines Schachbretts wollte er ein Korn, auf das zweite Feld das Doppelte, also zwei, auf das dritte wiederum die doppelte Menge, also vier und so weiter. Der König lachte und war gleichzeitig erbost über die vermeintliche Bescheidenheit des Brahmanen.

Als sich Shihram einige Tage später erkundigte, ob Sissa seine Belohnung in Empfang genommen habe, musste er hören, dass die Rechenmeister die Menge der Weizenkörner noch nicht berechnet hätten. Der Vorsteher der Kornkammer meldete nach mehreren Tagen ununterbrochener Arbeit, dass er diese Menge Getreidekörner im ganzen Reich nicht aufbringen könne. Nun stellte er sich die Frage, wie das Versprechen eingelöst werden könne. Der Rechenmeister half dem Herrscher aus der Verlegenheit, indem er ihm empfahl, er solle Sissa ibn Dahir ganz einfach das Getreide Korn für Korn zählen lassen.[^1]

[^1]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Sissa_ibn_Dahir)

::: task
#### ✏️ Aufgabe A

Du sollst die Menge Weizenkörner, welche Sissa vom Herrscher gefordert hat, berechnen. Gehe folgendermassen vor:

1. Erstelle eine neue Arbeitsmappe im Tabellenkalkulationsprogramm.
2. Schreibe die Zahl Eins ins Feld A1
3. Schreibe eine Formel ins Feld B1, welche die Zahl in A1 verdoppelt.
4. Fahre so weiter mit dem Feldern C1 bis H1, dann A2 bis H2, usw.
***
**Tipps:**
- Die Felder B2 bis H8 haben alle die «gleiche» Formel, hier kann automatisches Ausfüllen verwendet werden.
- Das gleiche gilt für die Felder A2 bis A8.
***
![](./chess-board-numbers.png)
:::

::: task
#### ✏️ Aufgabe B

Die Tausendkornmasse (Masse von 1000 Körnern) von Weizen beträgt 40 bis 65 Gramm. 2016 betrug die weltweite Jahresproduktion von Weizen 749'460'078 Tonnen. Berechne im Tabellenkalkulationsprogramm, wie lange die ganze Welt auf Weizen verzichten müsste, um die Forderung von Sissa ibn Dahir zu erfüllen.
***
Annahme: Tausendkorngewicht von 50g. Die Welt müsste über 1230 Jahre auf Weizen verzichten.
:::

::: task
#### ⭐ Zusatzaufgabe C

Formatiere den Zellbereich A1:H8 wie ein Schachbrett:
- Jedes zweite Feld hat eine scharzen Hintergrund und eine weisse Schriftfarbe.
- Die Felder sind quadratisch.

![](./chess-board.png)
***
**Tipps:**
- Zuerst alle Felder mit einer Farbe einfärben.
- Experten könnten eine bedingte Formatierung mit folgender Formel verwenden:

```
=REST(SPALTE(A1)+ZEILE(A1);2)>0
```
***
* [:file-excel: Weizenkornlegende](./sissa-loesung.xlsx)
:::
