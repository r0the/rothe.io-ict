# 3.3 Automatisches Ausfüllen
---

Mit dem **automatischen Ausfüllen** können Tabellenbereiche schnell mit einem vordefinierten Wert oder eine Formel ausgefüllt werden.

Um einen Tabellenbereich mit einem konstanten Wert zu füllen, wird dieser Wert in die erste Zelle des Bereichs geschrieben. Anschliessend wird das kleine Quadrat unten rechts der Zellmarkierung gefasst und in die gewünschte Richtung gezogen.

![Automatisches Ausfüllen mit einem konstanten Wert](./fill.png)

Ein Tabellenbereich kann auch mit einer auf- oder absteigenden Folge von Werten gefüllt werden. Dazu werden die zwei ersten Werte in zwei benachbarte Zellen geschrieben. Anschliessend werden **beide** Zellen ausgewählt. Nun wird wieder das kleine Quadrat unten rechts der Zellmarkierung gefasst und in die gewünschte Richtung gezogen.

![Automatisches Ausfüllen mit einer Differenz](./fill-diff.png)
