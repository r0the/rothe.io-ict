# Aufgabe Zellbezüge
---

Verwende für diese Aufgabe die untenstehende Arbeitsmappe «zellbezuege»:

* [:xlsx: Zellbezuege](./zellbezuege.xlsx)

::: exercise Aufgabe 12 – Telefontarif

In der Zelle B3 ist der Minutentarif für Telefongespräche eingetragen.

- Schreibe in die Zelle B6 eine Formel, um den Preis zu berechnen.
- Verwende *automatisches Ausfüllen*, um die Formel in die Zellen B7 bis B12 zu kopieren. Was passiert?
- Gib der Zelle B3 einen Namen und verwende diesen Namen in der Formel in Zelle B6
- Verwende wieder *automatisches Ausfüllen*, um die Formel in die Zellen B7 bis B12 zu kopieren. Was passiert?
- Ändere den Minutentarif. Was passiert?
:::

::: exercise Aufgabe 13 – Währungen

- Gib den Zellen B3 bis E3 einen Namen.
- Schreibe in die Zelle B6 eine Formel, um den Preis zu berechnen.
- Verwende *automatisches Ausfüllen*, um die Formel in alle Zellen bis E12 zu kopieren. Was passiert?
:::
