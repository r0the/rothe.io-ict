# Aufgabe Sporttag
---

::: exercise Aufgabe 11 – Sporttag
Speichere die Arbeitsmappe «sporttag» auf Deinem Computer:

* [:xlsx: Sporttag](./sporttag.xlsx)

- Die Punktzahl beim Hochsprung ergibt sich aus der Höhe multipliziert mit einem Faktor. Gib in den Zellen der Spalte F Formeln ein, um die Punktzahl für jede/n Teilnehmer/in zu berechnen.

- Beim Weitsprung wird nur der beste Versuch berücksichtigt. Verwende in der Spalte K eine Funktion, um den besten Sprung für jede/n Teilnehmer/in zu bestimmen.

- Das Punktetotal ergibt sich, indem die Punkte aus Hochsprung und Weitsprung addiert werden. Füge in Spalte O entsprechende Formeln ein.

- Sortiere die Tabelle nach dem Punktetotal absteigend.

- Schreibe den Rang in die Spalte A.
:::
