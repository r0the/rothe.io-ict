# Formeln und Zellbezüge
---

## Inhalt

* [:book: Formeln](?page=1-formulas/)
* [:book: Funktionen](?page=2-functions/)
* [✏️ Aufgabe Sporttag](?page=3-exercise/)
* [:book: Automatisches Ausfüllen](?page=4-autofill/)
* [✏️ Aufgabe Zellbezüge](?page=5-references/)
* [✏️ Aufgabe Weizenkornlegende](?page=6-sissa/)
