# Serienbriefe
---

Als Anwendung schauen wir uns einen Serienbrief an. Dabei erstellt man für jeden Datensatz einer Tabelle einen Brief und greift dazu auf die gespeicherten Werte des jeweiligen Datensatzes zu.

Um einen Serienbrief zu erstellen, müssen die folgenden drei Arbeitsschritte ausgeführt werden:

- Tabelle mit geeigneten Titeln erstellen
- Fertige Tabelle lokal abspeichern
- Textdokument mit Bezugsfeldern erstellen

## 1 Tabelle erstellen

Beim Erstellen der Datentabelle in Excel sollte darauf geachtet werden, dass geeignete Spaltenüberschriften gewählt werden:

![](./table.png)

Die Tabelle muss anschliessend lokal gespeichert werden, idealerweise im gleichen Ordner, wo auch das Word-Dokument für den Seriendruck gespeichert wird.

## 2 Word-Dokument erstellen

Der eigentliche Seriendruck findet in Word statt. Also wird nun Word gestartet und ein neues Dokument erstellt.

## 3 Seriendruck aktivieren
Um in einem Word-Dokument den Seriendruck zu aktivieren, wird der Menüpunkt _Sendungen ‣ Seriendruck_ starten ausgewählt:

![](./step-3.png)

Anschliessend kann die gewünschte Dokumentart (z.B. Briefe) ausgewählt werden.

## 4  Empfänger auswählen
Als nächstes muss die Excel-Tabelle mit den Empfängern ausgewählt werden. Die geschieht über den Menüpunkt _Sendungen ‣ Empfänger auswählen ‣ Vorhandene Liste verwenden…_:

![](./step-4.png)

Anschliessend kann die gewünschte Excel-Datei mit den Adressen ausgewählt werden.

## 5  Seriendruckfelder einfügen
Nun können die Spalten aus der Excel-Tabelle als Seriendruckfelder eingefügt werden. Unter dem Menüpunkt _Sendungen ‣ Seriendruckfeld einfügen_ steht eine Liste aller vorhandenen Spalten(-überschriften) zu Verfügung:

![](./step-5.png)

## 6  Vorschau aktivieren
Mit dem Menüpunkt _Sendungen ‣ Vorschau Ergebnisse_ kann die Vorschau ein- und ausgeschaltet werden. So kann überprüft werden, wie die fertigen Dokumente für jeden einzelnen Empfänger aussehen werden.

![](./step-6.png)

## 7  Drucken
Mit dem Menüpunkt _Sendungen ‣ Fertig stellen und zusammenführen_ kann der fertige Serienbrief gedruckt werden:

![](./step-7.png)

Wählt man _Einzelne Dokumente bearbeiten…_, so wird eine neue Worddatei erstellt, die sämtliche Briefe enthält. So können die einzelnen bei Bedarf noch angepasst werden.

::: exercise Aufgabe Serienbrief
1. Erstellen Sie ein Excel-Arbeitsmappe mit den folgenden Spalten:
    - Vorname
    - Nachname
    - Adresse
    - PLZ
    - Ort
    - Geschlecht

2. Erstellen Sie mindestens fünf Datensätze (also Zeilen mit Werten).
3. Erstellen Sie in der Cloud einen neuen Ordner __Seriendruck__.
4. Speichern Sie die Arbeitsmappe in diesem Ordner ab.
5. Starten Sie Word und erstellen Sie ein neues Dokument. Speichern Sie dieses Dokument auch im Ordner __Seriendruck__ ab.
6. Erstelle Sie einen Serienbrief gemäss Anleitung.
:::
