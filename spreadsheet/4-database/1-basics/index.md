# Einführung
---
s
Eine Datenbank ist ein System zur elektronischen Datenverwaltung. Die wesentliche Aufgabe einer Datenbank ist es, grosse Datenmengen effizient, widerspruchsfrei und dauerhaft zu speichern und benötigte Teilmengen in unterschiedlichen, bedarfsgerechten Darstellungsformen für Benutzer und Anwendungsprogramme bereitzustellen.

![](./database.svg)


- **Schema**: Gibt vor, was in der Datenbank wie abgespeichert werden soll
- **Attribut**: Beschreibt einen einzelnen Eintrag des Schemas – z.B. dass die Postleitzahl als Zahl erfasst werden muss
- **Datensatz**: Ein Eintrag in der Datenbank – hier im Beispiel eine spezifische Person, z.B. Jakob
- **Wert**: Der Wert eines Attributs – Der Wert für das Attribut Adresse des Datensatzes von Jakob lautet «Neubrückstrasse 125»
