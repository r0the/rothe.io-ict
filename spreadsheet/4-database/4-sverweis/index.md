# Tabellenverknüpfung
---

Mit der Funktion `SVERWEIS` können Tabellen verknüpft werden. Für einen Serienbrief haben wir die Tabellen «Person»

![](./table-person.png)

und «Geschlecht»:

![](./table-geschlecht.png)

Nun möchten wir aufgrund der Spalte «Geschlecht» in der Tabelle «Person» die Anreden aus der Tabelle «Geschlecht» automatisch in der Tabelle «Person» ergänzen.

## Funktion `SVERWEIS`

Dazu verwenden wir die Funktion `SVERWEIS`. In der Zelle G2 der Tabelle «Person» schreiben wir:

```
=SVERWEIS(F2; Geschlecht!$A$2:$C$3; 2; FALSCH)
```

Die Argumente der Funktion haben folgende Bedeutung:

- **Eingabewert** (`F2`): Dieser Wert ist der Schlüssel für das Geschlecht, wir wollen wir in der Tabelle Geschlecht suchen.
- **Tabelle** (`Geschlecht!$A$2:$C$3`): Hier wird angegeben, wo sich die Tabelle befindet, in welcher nachgeschaut werden soll. Die Tabelle befindet sich im Tabellenblatt «Geschlecht» im Zellbereich A2 bis C3. Der Name des Tabellenblatts wird von dem Zellbereich mit einem Ausrufezeichen `!` getrennt. Die Zellbezeichnungen werden mit Dollarzeichen `$` geschrieben, damit wir die Formel kopieren können, ohne dass die Tabelle verrutscht.
- **Spaltenindex** (`2`): Die Spaltennummer im Tabellenbereich, wo wir den gewünschten Wert finden.
- **Ähnliche Werte** (`FALSCH`): Excel kann auch «ähnliche» Werte finden. Das macht für unsere Anwendung aber keinen Sinn, deshalb muss hier in jedem Fall `FALSCH` stehen.
