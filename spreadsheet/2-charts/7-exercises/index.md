# Aufgaben
---

::: exercise Aufgabe Nationalratswahlen 1

Verwenden Sie für diese Aufgabe die folgende Excel-Arbeitsmappe:

* [:xlsx: Nationalratswahlen.xlsx](./Nationalratswahlen.xlsx)

Erstellen Sie die fünf Diagramme, auf der Seite [Säulendiagramme](?page=../2-bar-chart/) abgebildet sind.

1. Säulendiagramm der Nationalratswahlen von 2019 mit Parteiabkürzungen
2. Balkendiagramm der Nationalratswahlen von 2019 mit vollständigen Parteibezeichnungen
3. Säulendiagramm der Nationalratswahlen von 2015 und 2019
4. gestapeltes Säulendiagramm der Nationalratswahlen nach Geschlecht
4. gestapeltes Säulendiagramm 100% der Nationalratswahlen nach Geschlecht

***

1. Das Diagramm sollte so aussehen:

![](./diagram-nr-bar-v.png)

2. Das Diagramm sollte so aussehen:

![](./diagram-nr-bar-h.png)

3. Das Diagramm sollte so aussehen:

![](./diagram-nr-bar-grouped.png)

4. Das Diagramm sollte so aussehen:

![](./diagram-nr-bar-stacked.png)

:::

::: exercise Aufgabe Nationalratswahlen 2

1. Ergänzen Sie die Diagramme aus der vorherigen Aufgabe mit je einem passenden **Diagrammtitel**.
2. Fügen Sie in jedem Diagramm mit absoluten Werten den **Achsentitel** «Mandate» ein.
:::


::: exercise Aufgabe Nationalratswahlen 3

1. Stellen Sie die Mandatsverteilung der Nationalratswahlen von 2019 wie auf der Seite [Kreisdiagramme](?page=../4-pie-chart/) gezeigt in einem Kreisdiagramm dar. Stellen Sie sicher, dass die Flächen mit der Anzahl Mandaten und der Partei angeschrieben sind.
2. Stellen Sie die Geschlechterverteilung im Nationalrat von 2019 in einem Kreisdiagramm dar.

***

1. Das Diagramm sollte so aussehen:

![](./diagram-nr-pie-party.png)

2. Das Diagramm sollte so aussehen:

![](./diagram-nr-pie-gender.png)

:::

::: exercise Aufgabe COVID-19

Verwenden Sie für diese Aufgabe die folgende Excel-Arbeitsmappe:

* [:xlsx: Covid.xlsx](./covid.xlsx)

In der Tabelle **Daten** sind die weltweiten Fallzahlen von COVID-19 nach Land aufgelistet.

1. Wählen Sie die Tabelle **USA** aus und erstellen Sie dort ein **Flächendiagramm** der Fallentwicklung.
2. Betrachten Sie die Tabelle **Schweiz - Italien**, wo ein **gruppiertes Liniendiagramm** verwendet wird, um die Fallzahlen der beiden Länder zu vergleichen.
3. Wählen Sie zwei andere Länder. Kopieren Sie die Datenreihen der Länder in eine neue Tabelle und erstellen Sie dort ein analoges Diagramm, um die Fallzahlen der beiden Länder zu vergleichen.

:::

::: exercise Aufgabe Weltbevölkerung

Verwenden Sie für diese Aufgabe die folgende Excel-Arbeitsmappe:

* [:xlsx: Weltbevoelkerung.xlsx](./weltbevoelkerung.xlsx)

1. Erstellen Sie aus den Daten wie in der Datei beschrieben ein Liniendiagramm und ein Punktdiagramm.
2. Legen Sie beim Punktdiagramm folgende Achsonoptionen fest:
   - horizontale Achse: Minimum auf 0 und Maximum auf 2015.
   - vertikale Achse: Anzeigeeinheit **Milliarden**.
2. Vergleichen Sie die beiden Diagramme.
3. Mit welchem Diagramm verbinden Sie den Begriff **exponentielles Wachstum**?
4. Welches Diagramm vermittelt die Realität besser?

***

Das Punktdiagramm sollte so aussehen:

![](./diagram-pop-world-xy.png)

Das Liniendiagramm sollte so aussehen:

![](./diagram-pop-world-line.png)

:::

::: extra Zusatzaufgabe Klimadiagramm

Um ein kombiniertes Diagramm (Balken und Linien) zu erstellen, im Dialog «Diagramm einfügen» im Tab Alle Diagramme den Diagrammtyp **Verbund** auswählen.
- Die Temperaturskala wird auf der **Primärachse** (links) dargestellt.
- Die Niederschlagsskala wird auf der **Sekundärachse** (rechts) dargestellt.
0 Minimum und Maximum der beiden Skalen müssen so gewählt werden, dass die eine Skala ein Vielfaches der anderen Skala ist. Beispielsweise wird für die Temperatur eine Skala von -10 °C bis 60 °C gewählt und für den Niederschlag das Doppelte, also von -20 mm bis 120 mm.

* [:xlsx: Klimadiagramm.xlsx](./klimadiagramm.xlsx)

![](./klimadiagramm.png)
:::
