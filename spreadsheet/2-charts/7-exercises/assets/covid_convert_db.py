#!/usr/bin/env python3
import json
import openpyxl

weeks = set()
countries = {}

def parse_record(country_map, record):
    if not "country_code" in record:
        return
    id = record["country_code"]
    if id in IGNORE: 
        return
    week = record["year_week"]
    weeks.add(week)
    indicator = record["indicator"]
    value = int(record["weekly_count"])
    if not id in country_map:
        country_map[id] = {
            "name": record["country"],
            "cases": {},
            "deaths": {}
        }
        population = record["population"]
        if population:
            country_map[id]["population"] = int(population)

    data = country_map[id]
    data[indicator][week] = value

def parse_original_data():
    country_map = {}
    with open("covid.json") as file:
        records = json.load(file)
    for record in records:
        parse_record(country_map, record)
    return country_map

country_map = parse_original_data()

weeks = list(weeks)
weeks.sort()

countries = list(country_map.keys())
countries.sort()

wb = openpyxl.Workbook()
sheet = wb.active
sheet.cell(row=1, column=1).value = "Woche"
row = 2
for week in weeks:
    sheet.cell(row=row, column=1).value = week
    row = row + 1

column = 2
for country in countries:
    data = country_map[country]
    print(country)
    population = data["population"]
    sheet.cell(row=1, column=column).value = data["name"]
    row = 2
    cases = data["cases"]
    for week in weeks:
        if week in cases:
            sheet.cell(row=row, column=column).value = 100000 * cases[week] / population
        row = row + 1
    column = column + 1
 
wb.save("covid.xlsx")
