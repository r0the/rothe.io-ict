# Aufgaben 1
---

Verwende für diese Aufgaben die folgende Datei:

* [:file-excel: Diagramme](exercise/diagramme.xlsx)

::: exercise Aufgabe a) Schweizer Bevölkerung nach Nationalität

Wähle einen geeigneten Diagrammtyp, um die Daten darzustellen. Das Diagramm muss folgendes enthalten:

- aussagekräftiger Diagrammtitel
- Datenbeschriftungen
- Legende
:::

::: exercise Aufgabe b) Frauenanteil im Nationalrat nach Parteien

Wähle einen geeigneten Diagrammtyp, um die Daten darzustellen. Das Diagramm muss folgendes enthalten:

- aussagekräftiger Diagrammtitel
- Achsentitel für die vertikale Achse
- Legende
:::

::: exercise Aufgabe c) Zeitliche Entwicklung der Nationalratsmandate nach Parteien

Wähle einen geeigneten Diagrammtyp, um die Daten darzustellen. Das Diagramm muss folgendermassen gestaltet sein:

- aussagekräftiger Diagrammtitel
- Achsentitel für die vertikale Achse
- Legende
- Die horizontale Achse muss von 1931 bis 2015 reichen und muss in Intervalle von vier Jahren unterteilt sein.
:::

::: exercise Aufgabe d) Entwicklung der Weltbevölkerung

Im Tabellenblatt sind zwei Tabellen zur Enwicklung der Weltbevölkerung vorhanden. Erstelle aus der linken Tabelle **Liniendiagramm** und aus der rechten Tabelle ein **Punktdiagramm**. Beide Diagramme sollen so gestaltet werden:

- aussagekräftiger Diagrammtitel
- Achsentitel für **beide** Achsen
- Die horizontale Achse muss bei 0 beginnen und bei 2015 enden. (Beim Punktdiagramm muss die Zahl 2015 auf der Achse nicht angezeigt werden, aber der Datenpunkt muss vorhanden sein).
- Die vertikale Achse soll die Anzeigeeinheit «Millionen» haben.

Vergleiche die beiden Diagramme.

- Wieso sehen die Diagramme unterschiedlich aus?
- Welches ist aussagekräftiger und wieso?
:::

::: extra Zusatzaufgabe

Normalerweise sind Daten für Diagramme nicht fertig aufbereitet verfügbar. Für die folgende Aufgabe musst du die Daten selbst finden und in eine geeignete Tabelle überführen.

Erstelle die folgenden Diagramme:

- Zeitliche Entwicklung der Maturitätsabschlüsse in der Schweiz, nach Geschlecht
- Maturitätsabschlüsse in der Schweiz im Jahr 2016, Verteilung nach Schwerpunktfach

Tipp: Du findest die nötigen Tabelle auf der Webseite des [Bundesamtes für Statistik](https://www.bfs.admin.ch/bfs/de/home/statistiken.html).
:::
