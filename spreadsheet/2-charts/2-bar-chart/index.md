# 2.2 Säulendiagramme
---

Mit einem Säulen- oder Balkendiagramm (engl. *bar chart*) werden absolute Zahlen dargestellt. Bei dieser Darstellung können die Unterschiede zwischen den einzelnen Werten schnell erkannt werden.

## ![](./icon/bar-v-2d-grouped.png) Säulendiagramm

In einem **Säulendiagramm** werden die Werte durch vertikale Säulen dargestellt. Eine **Skala** am linken Rand ermöglicht es, die Werte abzulesen.

![Hier wird dargestellt, welche Partei wie viele Mandate erhalten hat. Die Anzahl Mandate können abgelesen und verglichen werden.](./diagram-nr-bar-v.png)


## ![](./icon/bar-h-2d-grouped.png) Balkendiagramm

In einem **Balkendiagramm** werden die Werte durch horizontale Balken dargestellt. Die Skala befindet sich am unteren Rand des Diagramms. Die Verwendung eines Balkendiagramms ist sinnvoll, wenn die Rubriken lange Bezeichnungen haben.

![Hier wird ein Balkendiagramm verwendet, weil die ausgeschriebenen Parteinamen nebeneinander nicht Platz haben.](./diagram-nr-bar-h.png)

## mehrere Datenreihen

In einem Säulendiagramm können auch mehrere Datenreihen dargestellt werden. Dabei gibt es drei Varianten:

### ![](./icon/bar-v-2d-grouped.png) gruppierte Datenreihen


![Hier werden die Mandate pro Partei von 2015 und 2019 miteinander verglichen. Es ist ersichtlich, welche Partei Sitze gewonnen und verloren hat.](./diagram-nr-bar-grouped.png)


### ![](./icon/bar-v-2d-stacked.png) gestapelte Datenreihen

Wenn die Werte der Datenreihen zu einem Gesamtsumme addiert werden können, eignen sich gestapelte Säulen oder Balken:

![](./diagram-nr-bar-stacked.png)

### ![](./icon/bar-v-2d-relative.png) gestapelte Datenreihen 100%

![](./diagram-nr-bar-relative.png)


## Diagramm erstellen

<!-- <v-video id="2RbKVv7RTdQ"/> -->

Gehe so vor, um ein Säulen- oder Balkendiagramm zu erstellen:

1. Wähle den gewünschten Bereich mit den Daten aus.
2. Wähle das Menüband _Einfügen_.
3. Klicke auf das Icon für Säulendiagramme.
4. Wähle den gewünschten Diagrammtyp aus.

Beachte dabei folgendes:

- Du kannst mit [Ctrl] (macOS: [Command]) auch nicht zusammenhängende Bereiche auswählen.
- Das gehört **nicht** zu den Daten, es darf **nicht ausgewählt** werden.

![](./excel-diagram-bar-create.svg)

### Diagrammtyp

Die Icons für die Diagrammtypen haben folgende Bedeutung:

|           | gruppiert                        | gestapelt                        | gestapelt 100%                    |
|:--------- |:-------------------------------- |:-------------------------------- |:--------------------------------- |
| 2D-Säule  | ![](./icon/bar-v-2d-grouped.png) | ![](./icon/bar-v-2d-stacked.png) | ![](./icon/bar-v-2d-relative.png) |
| 3D-Säule  | ![](./icon/bar-v-3d-grouped.png) | ![](./icon/bar-v-3d-stacked.png) | ![](./icon/bar-v-3d-relative.png) |
| 2D-Balken | ![](./icon/bar-h-2d-grouped.png) | ![](./icon/bar-h-2d-stacked.png) | ![](./icon/bar-h-2d-relative.png) |
| 3D-Balken | ![](./icon/bar-h-3d-grouped.png) | ![](./icon/bar-h-3d-stacked.png) | ![](./icon/bar-h-3d-relative.png) |
