# Linien- und Flächendiagramme
---

Mit Linien- und Flächendiagrammen können zeitliche Entwicklungen dargestellt werden.

Wie bei den Säulen- und Balkendiagrammen können mehrere Datenreihen entweder nebeneinander, gestapelt oder relativ dargestellt werden. Dabei verwenden wir immer ein

- **Liniendiagramm** für gruppierte Datenreihen und
- **Flächendiagramm** für gestapelte Datenreihen und relative Darstellung (100%).

## ![](./icon/line-grouped.png) Liniendiagramm

Beim Liniendiagramm werden die einzelnen Datenwerte durch eine gerade Linie miteinander verbunden. Dadurch entsteht der Eindruck einer zeitlichen Entwicklung.

**Achtung:** Diese Darstellung suggeriert, dass sich die Werte kontinuierlich verändern. Im Fall der Mandatsverteilung ist dies natürlich falsch. Diese bleibt immer während vier Jahren gleich.

![](./diagram-nr-line-grouped.png)

## ![](./icon/line-p-grouped.png) Liniendiagramm mit Datenpunkten

Beim Liniendiagramm mit Datenpunkten werden die tatsächlich vorhandenen Werte zusätzlich durch Punkte markiert.

![](./diagram-nr-line-p-grouped.png)

## ![](./icon/area-stacked.png) Flächendiagramm gestapelt

Wenn die Werte mehrere Datenreihen addiert werden können, dann eignet sich ein gestapeltes Flächendiagramm:

![Hier werden die Mandate der SP zwischen 1970 und 2019 dargestellt, aufgeteilt nach Geschlecht.](./diagram-nr-area-stacked.png)

## ![](./icon/area-relative.png) Flächendiagramm gestapelt 100%

Beim gestapelten Flächendiagramm werden die Werte der verschiedenen Datenreihen relativ, also prozentual dargestellt. Im Gegensatz zum «normalen» gestaptelen Flächendiagramm ist hier die Veränderung der Mandate der Partei nicht mehr ersichtlich. Dafür ist die Veränderung des Frauenanteils besser erkennbar.

![Hier werden die Mandate der SP zwischen 1970 und 2019 dargestellt, aufgeteilt nach Geschlecht.](./diagram-nr-area-relative.png)
