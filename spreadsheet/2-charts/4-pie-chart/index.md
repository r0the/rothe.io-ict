# Kreis- und Ringdiagramme
---

Ein Kreis- oder Ringdiagramm (auch «Kuchendiagramm», engl. *pie chart*) wird verwendet, um **relative Verhältnisse** darzustellen.

In einem Kreisdiagramm kann nur eine Datenreihe sinnvoll dargestellt werden.

Beim Kreisdiagramm sollten die **Datenbeschriftungen** angezeigt werden, da die Werte nicht an einer Skala abgelesen werden können.


![](./diagram-nr-pie-party.png)

## Datenbeschriftungen

Klicke auf das Plus-Symbol, um das Menü _Diagrammelemente_ anzuzeigen.
Klicke anschliessend auf den Pfeil _‣_ rechts vom Menüpunkt _Datenbeschriftungen_. Wähle nun die gewünschte Position der Beschriftung:

- _Zentriert_: die Beschriftung wird innerhalb der Fläche zentriert dargestellt
- _Am Ende innerhalb_: die Beschriftung wird innerhalb der Fläche am Rand dargestellt
- _Am Ende ausserhalb_: die Beschriftung wird ausserhalb der Fläche dargestellt

![](./excel-data-labels.png)
