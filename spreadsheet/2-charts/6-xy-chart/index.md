# Punktdiagramme
---

Das Punktdiagramm ist der einzige Diagrammtyp, welcher auf beiden Achsen **Zahlenwerte** darstellt.

Wir setzen ein Punktdiagramm bei zeitlichen Veränderungen ein, wenn die zeitlichen Abstände **nicht gleichmässig** sind.

Im folgenden Beispiel wollen wir die Entwicklung der Schweizer Wohnbevölkerung als Diagramm darstellen. Von 1950 bis 1990 gibt es nur alle 10 Jahre entsprechende Daten. Seit 1995 liegen jährliche Daten vor. Der zeitliche Abstand der Datenwerte ist also nicht einheitlich, deshalb muss ein Punktdiagramm verwendet werden:

![Schweizer Wohnbevölkerung zwischen 1950 und 2016](./diagram-pop-ch-xy.png)

Wenn wir die gleichen Daten als Liniendiagramm darstellen würden, sähe das so aus:

![Schweizer Wohnbevölkerung als Liniendiagramm](./diagram-pop-ch-line.png)

Dieses Diagramm vermittelt das falsche Bild, dass das Bevölkerungswachstum ab 1995 abflacht, was nicht stimmt.

## Achsen formatieren

Bei Punktdiagrammen muss man normalerweise das Minimum und Maximum auf der horizontalen Achse festlegen, damit die Darstellung stimmt. Gehe dazu so vor:

1. Achse auf dem Diagramm auswählen.
2. Menüband _Format_ auswählen.
3. _Formatbereich_ anklicken, um den Formatbereich anzuzeigen.
4. Tab _Achsenoptionen_ wählen.
5. Werte für _Minimum_ und _Maximum_ eingeben.
6. Optional kann für die unter _Anzeigeeinheiten_ z.B: _Millionen_ ausgewählt werden, falls grosse Zahlen dargestellt werden.

![](./excel-format-axis.svg)
