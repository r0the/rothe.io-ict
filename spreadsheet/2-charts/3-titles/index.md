# Beschriftungen
---

## Diagrammtitel


Klicke auf das Plus-Symbol, um das Menü _Diagrammelemente_ anzuzeigen. Setze beim Menüpunkt _Diagrammtitel_ einen Haken, falls keiner vorhanden ist.

![](./excel-diagram-title.png)

Klicke anschliessend auf den umrahmten Diagrammtitel um diesen abzuändern.

Durch das Ziehen am Rahmen des Titels kann dieser positioniert und in seiner Grösse angepasst werden.

## Achsentitel

Klicke auf das Plus-Symbol, um das Menü _Diagrammelemente_ anzuzeigen.
Klicke anschliessend auf den Pfeil _‣_ rechts vom Menüpunkt _Achsentitel_. Setze im nun erscheinenden Untermenü beim gewünschten Menüpunkt einen Haken.

- _Primär horizontal_: Achsentitel für die horizontale Achse
- _Primär vertikal_: Achsentitel für die vertikale Achse

![](./excel-axis-title.png)

Klicke anschliessend auf den umrahmten Achsentitel um diesen abzuändern.

Durch das Ziehen am Rahmen des Titels kann dieser positioniert und in seiner Grösse angepasst werden.
