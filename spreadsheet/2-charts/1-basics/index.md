# 2.1 Begriffe
---

## Kategorien und Datenreihen

Ein Diagramm ist die grafische Darstellung von Daten. Dabei werden normalerweise mehrere Datensätze (Zeilen) miteinander verglichen.

Für ein Diagramm muss ein Feld (eine Spalte) ausgewählt werden, welche den **Namen** des Datensatzes darstellt. Dieses Feld wird als **Rubrik** oder **Kategorie** bezeichnet.

Ausserdem wird ein oder mehrere Felder ausgewählt, in welchen sich **Zahlen** befinden. Diese Spalten werden **Datenreihen** genannt.

Beispielsweise werden für Parteien (die Kategorien) die Mandate in verschiedenen Nationalratswahlen (die Datenreihen) verglichen:

![](./diagram-series.png)

Beim Säulendiagramm werden die Kategorien auf der horizontalen Achse abgebildet, die Datenreihen auf der vertikalen.

## Aufbau eines Diagramms

Ein aussagekräftiges Diagramm enthält mindestens

- einen **Diagrammtitel**,
- einen **Achstitel** für die Datenreihen,
- **Gitternetzlinien**, um  die Werte der Datenreihen gut ablesen zu können,
- eine **Legende**, sofern mehr als eine Datenreihe dargestellt wird.

![](./diagram-parts.png)
