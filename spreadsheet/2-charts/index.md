# 2 Diagramme
---

::: cards 2
[![](./diagram-nr-bar-grouped.png)][1]
#### [Säulen- und Balkendiagramme][1]
absolute Werte
***

[![](./diagram-nr-pie-party.png)][2]
#### [Kreis- und Ringdiagramme][2]
relative Verhältnisse
***

[![](./diagram-nr-area-stacked.png)][3]
#### [Linien- und Flächendiagramme][3]
zeitliche Entwicklung mit **gleichbleibenden Intervallen**
***

[![](./diagram-pop-ch-xy.png)][4]
#### [Punktdiagramme][4]
Zusammenhang zweier nummerischer Werte, zeitliche Entwicklung mit **unterschiedlichen Intervallen**
:::

[1]: ?page=2-bar-chart/
[2]: ?page=4-pie-chart/
[3]: ?page=5-line-chart/
[4]: ?page=6-xy-chart/
