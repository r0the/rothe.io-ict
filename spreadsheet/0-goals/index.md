# Lernziele
---

::: goal Lernziele Tabellen verwenden
- Du kennst die grundlegenden Elemente einer Tabellenkalkulation (Arbeitsmappen, Tabellenblätter, Zeilen, Spalten, Zelle).
- Du kannst Tabellen sortieren und filtern.
- Du kannst Tabellenbereiche auswählen und kopieren.
- Du kannst Zahlen sinnvoll formatieren (Tausendertrennzeichen, Nachkommastellen, wissenschaftliche Notation, Prozent).
:::

::: goal Lernziele Diagramme
- Du kennst die Diagrammtypen Kreis-, Säulen- und Linien- und Punktdiagramm.
- Du verstehst den Unterschied zwischen Linien- und Punktdiagrammen.
- Du kannst vorgegebene Daten in ein aussagekräftiges Diagramm überführen.
- Du kannst Diagramme anpassen (Diagrammtitel, Skalen, Achen- und Datenbeschriftungen)
:::

::: goal Lernziele Formeln und Zellbezüge
- Du kannst die Grundrechenoperationen in Formeln verwenden.
- Du verwendest relative und absolute Zellbezüge in Formeln.
- Du kannst einfache Funktionen verwenden (Wurzel, Summe, Max, Min, Mittelwert und Runden).
- Du setzt das automatische Ausfüllen ein, um Eingaben und Berechnungen zu automatisieren.
:::

::: goal Lernziele Datenbanken
- Du kannst die Begriffe «Datenbank», «Tabelle», «Feld», «Datensatz», «Datentyp» und «Schlüssel» erklären und verstehst deren Beziehung untereinander.
- Du erkennst Schlüsselfelder und berechnete Felder in einer Datenbanktabelle.
- Du kannst mit einer Tabellenkalkulationssoftware einfache Datenbanken verwalten.
:::

## Grundlagen

Diese Unterrichtseinheit deckt die folgenden Grobziele und Inhalte aus dem kantonalen Lehrplan[^1] ab:

> ### Information und Daten
>
> ### ICT
>
> #### Grobziele
> Die Schülerinnen und Schüler
> - setzen ein Office-Paket sachgerecht ein
>
> #### Inhalte
> - Grundlegende Gestaltungs- und Präsentationsprinzipien
> - Aufbereitung, Auswertung und Visualisierung von Daten

[^1]: Quelle: *[Lehrplan 17 für den gymnasialen Bildungsgang - Informatik][1], Erziehungsdirektion des Kantons Bern, S. 145 - 146*

[1]: https://www.erz.be.ch/erz/de/index/mittelschule/mittelschule/gymnasium/lehrplan_maturitaetsausbildung.html
