# :mdi-apple: Apple-Tastatur
---

In diesem Abschnitt werden die Besonderheiten von Apple-Tastaturen betrachtet.


![Apple MacBook-Tastatur mit Schweizer Layout](./keyboard-layout-de-ch-apple.svg)

Folgende Spezialtasten sind auf Apple-Tastaturen zu finden:

## Option-Taste

Die Option-Taste [Option] ist eine Kombinationstaste, mit welcher zusätzliche Sonderzeichen getippt werden können. Die meisten Zeichen, welche mit dieser Taste getippt werden können, sind nicht auf der Tastatur abgebildet.

**Beispiel:** Das @-Zeichen wird mit der Kombination [Option]+[G] getippt.

## Command-Taste

Die Command-Taste [Command] ist eine Kombinationstaste, mit welcher Befehle in der aktuellen App ausgelöst werden. In der folgenden Tabelle sind einige wichtige Kombinationen aufgeführt:

| Kombination     | Befehl                                    |
|:--------------- |:----------------------------------------- |
| [Command]+[A]   | alles markieren                           |
| [Command]+[C]   | Markierung in die Zwischenablage kopieren |
| [Command]+[X]   | Markierung ausschneiden                   |
| [Command]+[V]   | Inhalt aus der Zwischenablage einfügen    |
| [Command]+[Z]   | letzte Aktion rückgängig machen           |
| [Command]+[S]   | Dokument speichern                        |
| [Command]+[O]   | Dokument öffnen                           |
| [Command]+[P]   | Dokument drucken                          |
| [Command]+[F]   | Dokument durchsuchen                      |
| [Command]+[W]   | Fenster/Tab schliessen                    |
| [Command]+[H]   | Aktuelle App verstecken                   |
| [Command]+[Tab] | Wechsel zwischen Anwendungen              |
