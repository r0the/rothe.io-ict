# :mdi-microsoft-windows: Windows-Tastatur
---

In diesem Abschnitt werden die Besonderheiten von Windows-Tastaturen betrachtet.

![Schweizer Tastaturbelegung](../keyboard-layout-de-ch.svg)

Folgende Spezialtasten sind auf Windows-Tastaturen zu finden:

## Alternate Graphic-Taste

Die Alternate Graphic-Taste [AltGr] ist eine Kombinationstaste, mit welcher zusätzliche Sonderzeichen getippt werden können. Mit dieser Taste wird das Zeichen getippt, welches unten links auf der Taste abgebildet ist.

**Beispiel:** Das @-Zeichen wird mit der Kombination [AltGr]+[2] getippt.

## Steuerungstaste

Die Steuerungstaste [Ctrl] ist eine Kombinationstaste, mit welcher Befehle in der aktuellen App ausgelöst werden. Je nach Anwendung sind verschiedene Befehlskombinationen verfügbar, die folgenden sind aber Standard:

| Kombination  | Befehl                                    |
|:------------ |:----------------------------------------- |
| [Ctrl]+[A]   | alles markieren                           |
| [Ctrl]+[C]   | Markierung in die Zwischenablage kopieren |
| [Ctrl]+[X]   | Markierung ausschneiden                   |
| [Ctrl]+[V]   | Inhalt aus der Zwischenablage einfügen    |
| [Ctrl]+[Z]   | letzte Aktion rückgängig machen           |
| [Ctrl]+[N]   | neues Dokument erstellen                  |
| [Ctrl]+[S]   | Dokument speichern                        |
| [Ctrl]+[O]   | Dokument öffnen                           |
| [Ctrl]+[P]   | Dokument drucken                          |
| [Ctrl]+[F]   | Dokument durchsuchen                      |
| [Ctrl]+[Tab] | Wechsel zwischen Dokumenten (Tabs)        |
| [Ctrl]+[W]   | Fenster/Tab schliessen                    |

## Alternate-Taste

Die Alt- oder Alternate-Taste [Alt] ist ebenfalls eine Kombinationstaste, mit welcher Befehle ausgelöst werden können, beispielsweise die folgenden:

| Kombination | Befehl                       |
|:----------- |:---------------------------- |
| [Alt]+[F4]  | Anwendung schliessen         |
| [Alt]+[Tab] | Wechsel zwischen Anwendungen |

## Windows-Taste

| Kombination           | Befehl                                             |
|:--------------------- |:-------------------------------------------------- |
| [Windows]+[D]         | alle Fenster minimieren, Desktop anzeigen          |
| [Windows]+[Left]      | Fenster in der linken Bildschirmhälfte platzieren  |
| [Windows]+[Right]     | Fenster in der rechten Bildschirmhälfte platzieren |
| [Windows]+[Up]        | Fenster auf ganzen Bildschirm maximieren           |
| [Windows]+[L]         | Computer sperren                                   |
| [Windows]+[E]         | Dateiexplorer öffnen                               |
| [Windows]+[P]         | Präsentationsmodus umschalten                      |
| [Windows]+[Shift]+[S] | Bildschirmfoto erstellen                           |
