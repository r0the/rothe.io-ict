# Tastatur
---

## Tastaturbelegung

Als Tastaturbelegung oder Tastaturlayout bezeichnet man die Lage und Bedeutung der Tasten auf einer Computertastatur. Für viele Länder und Sprachen existieren eigene Tastaturbelegungen. Zusätzlich unterscheiden sich die Tastaturbelegungen von Windows- und Apple-Computern.

![Schweizer Tastaturlayout](./keyboard-layout-de-ch.svg)

Die Belegung einer Computertastatur kann im Betriebssystem festgelegt werden. Dieses wandelt die Signale der Tastatur in Eingaben um.


## Anordnung der Buchstaben

Auf den frühen mechanischen Schreibmaschinen waren die Buchstaben alphabetisch angeordnet. 1868 führte der US-amerikanische Drucker und Zeitungsherausgeber Christopher Latham Sholes die heute übliche Anordnung ein. Sein Ziel war es, Buchstaben, die häufig direkt nacheinander getippt werden, räumlich voneinander zu trennen. Dadurch lagen auch die **Typenhebel** dieser Buchstaben weiter auseinander und verhakten sich weniger oft[^1].

![Typenhebel einer mechanischen Tastatur ©](./typebar.jpg)

Sholes hatte die Anordnung der Tasten für die englische Sprache optimiert. Für die deutsche und die französische Sprache sind Abwandlungen entwickelt worden. Die drei Varianten werden oft als QWERTY (Englisch), QWERTZ (Deutsch) und AZERTY (Französisch) bezeichnet:

::: columns 3
![QWERTZ (Deutsch)](./qwertz.svg)
***
![QWERTY (Englisch)](./qwerty.svg)
***
![AZERTY (Französisch)](./azerty.svg)
:::

[^1]: Quelle: [Wikipedia – Tastaturbelegung](https://de.wikipedia.org/wiki/Tastaturbelegung#Geschichte)
