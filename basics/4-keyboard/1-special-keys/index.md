# Spezielle Tasten
---

## Insert-Taste

Die [Ins]- oder Insert-Taste schaltet zwischen dem Einfüge- und dem Überschreibmodus um.

Im heute üblichen **Einfügemodus** werden getippte Zeichen in den Text eingefügt, ohne den folgenden Text zu veränderen. Im **Überschreibmodus** werden schon vorhandene Zeichen im Text überschrieben. In vielen modernen Programmen wird dieser Modus nicht mehr verwendet.

## Löschtasten

Es gibt zwei Löschtasten:

- Die Delete-Taste [Del] löscht das Zeichen **rechts** des Cursors.
- Die Rückschritttaste [Backspace] (engl. *backspace*) löscht das Zeichen **links** des Cursors.

## Umschalttaste (Shift-Taste)

Die Umschalttaste oder Shift-Taste [Shift] ist eine **Kombinationstaste**. Das Drücken dieser Taste alleine hat keinen Effekt. Wir sie aber gedrückt, während Buchstaben getippt werden, werden diese als Grossbuchstaben getippt.

Bei den Tasten für Ziffern und Sonderzeichen wird ein alternatives Zeichen getippt, das oben oder oben links auf der Taste abgebildet ist.

## Feststelltaste (Capslock)

Die Feststelltaste [CapsLock] schaltet dauerhaft zwischen der Eingabe von Klein- und Grossbuchstaben um. Im Unterschied zur Umschalttaste werden die Tastten für Ziffern und Sonderzeichen nicht umgeschaltet.

## Tottasten

Eine Spezialität der Schweizer Tastatur sind die sogenannten Tottasten. Sie sind auf der Abbildung gelb dargestellt. Mit Hilfe dieser Tasten können *Akzente* mit verschiedenen Buchstaben kombiniert werden. Das sind kleine Zeichen wie Punkte, Striche oder Häkchen, welche an Buchstaben angebracht sind.

| Taste       | Akzent     | Beispiel | Verwendung                                        |
|:----------- |:---------- |:-------- |:------------------------------------------------- |
| [¨]         | Trema      | ä        | Umlaute in der deutschen Sprache                  |
| [AltGr]+['] | Akut       | é        | «accent aigu» in der französischen Sprache        |
| [Shift]+[^] | Gravis     | è        | «accent grave» in der französischen Sprache       |
| [AltGr]+[^] | Tilde      | ñ        | auf dem «n» in der spanischen Sprache             |
| [^]         | Zirkumflex | ê        | «accent circonflexe» in der französischen Sprache |

Um ein Buchstabe mit einem Akzent zu verstehen, wird erst die Tottaste getippt (und wieder losgelassen), anschliessend der gewünschte Buchstabe. Um beispielsweise den Buchstaben «ë» zu erzeugen, wird erst [¨] getippt und anschliessend [E].
