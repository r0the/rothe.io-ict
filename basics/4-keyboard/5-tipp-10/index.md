# TIPP 10
---

TIPP 10 ist ein Online-Tool zum Erlernen des Zehnfingersystems. Es ist gratis und sehr zugänglich:

* [:link: TIPP 10 Online](https://online.tipp10.com/)


## Einstellungen

Im Tab _Einstellungen_ sollte folgendes angepasst werden:

- __Tastaturlayout__ muss auf _Schweiz SG Windows_ oder _Schweiz SG Macintosh_ eingestellt werden. Damit wird sichergestellt, dass in den Lektionen alle Vorkommen das Zeichens «ß» durch «ss» ersetzt werden.
- __Lektionen__ sollte auf _Deutsch QWERTZ_ eingestellt werden.

Danach muss aus __Einstellungen speichern__ geklickt werden.

## Lektion starten

Vor dem Start einer Lektion sollten die Hilfestellungen überprüft werden. Im Unterricht muss unbedingt das _Akustische Signal_ und das _Metronom_ ausgeschaltet werden:

![](./tipp-10-start.png)
