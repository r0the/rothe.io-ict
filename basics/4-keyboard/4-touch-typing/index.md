# Zehnfingersystem
---

Das Zehnfingersystem ist eine Technik, um schnell auf der Tastatur zu schreiben. Dabei ist jedem Finger eine Grundposition auf der Tastatur zugeordnet. Jede Taste wird immer mit dem gleichen Finger getippt, so dass man sich die Griffwege beim Erlernen einprägen kann.

## Schreibgeschwindigkeit

Die erreichbare Schreibgeschwindigkeit ist von der Trainingszeit sowie der persönlichen Fähigkeit abhängig. Geübte Zehnfingerschreiber erreichen bei einem 10-Minuten-Test 200 bis 400 Anschläge pro Minute. Auf internationalen Wettbewerben werden derzeit bis zu 900 Anschläge pro Minute erreicht[^1].

## Grundstellung

In der Grundstellung befinden sich alle Finger auf den Grundtasten. Die Zeigefinger befinden sich auf den Tasten [F] und [J]. Auf den meisten Tastaturen befindet sich auf diesen Tasten eine kleine Erhebung, damit sie blind gefunden werden können. Die Daumen befinden sich auf der Leertaste.

![Grundposition für das Zehnfingersystem](./touch-typing-base.svg)


## Zuordnung der Tasten

Jede Taste ist einem bestimmten Finger zugeordnet und wird im Zehnfingersystem immer mit diesem Finger getippt:

![Zuordnung der Tasten zu den Fingern](./touch-typing-all.svg)


[^1]: Quelle: [Wikipedia – Zehnfingersystem](https://de.wikipedia.org/wiki/Zehnfingersystem)
