# ⭐ Neo-Tastaturbelegung
---

Neo ist eine moderne, ergonomische Tastaturbelegung für die deutsche Sprache. Die ist dafür ausgelegt, möglichst entspannt mit dem Zehnfingersystem tippen zu können und möglichst viele Zeichen darstellen zu können.

![Erste Ebene der Neo-Tastaturbelegung](./neo-2-layer-1.svg)

Dazu hat die Neo-Tastaturbelegung sechs Ebenen, welche mit Kombinationstasten erreicht werden können. Die zweite Ebene erreicht man mit der normalen Umschalttaste [Shift]:

![Zweite Ebene der Neo-Tastaturbelegung](./neo-2-layer-2.svg)

## Anwendung

Es gibt keine physischen Tastaturen, welche die Neo-Tastaturbelegung wiedergeben. Sie ist jedoch auf allen modernen Betriebssystemen als Tastaturlayout verfügbar.
