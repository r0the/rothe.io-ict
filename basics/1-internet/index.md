# 1 Internet
---

## Wichtige Links

* [:link: Elektronisches Klassenbuch](https://intern.gymkirchenfeld.ch)
* [:link: ICT am Gymnasium Kirchenfeld](https://ict.mygymer.ch)
* [:link: LearningView](https://learningview.org)

## Inhalt

* [1.1 - Browser](?page=1-browser/)
* [1.2 - Suchmaschinen](?page=2-search-engine/)
* [1.3 - Links / URLs](?page=3-links/)
* [1.4 - E-Mail verwenden](?page=4-e-mail-usage/)