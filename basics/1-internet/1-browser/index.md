# 1.1 Browser
---

## Begriff

Ein Webbrowser ist ein Programm, welches auf dem lokalen Gerät installiert ist. Es fordert als die für eine Webseite benötigten Dateien von einem *Webserver* an. Anschliessend analysiert es diese Dateien und stellt die Webseite entsprechend dar.

Der Browser führt auch Programme, welche in der Webseite enthalten sind, aus.

## Produkte

Aktuell dominieren vier Browser den Markt.

|                          | Name    | Hersteller         |
|:------------------------ |:------- |:------------------ |
| ![](./logo-chrome.png)   | Chrome  | Google             |
| ![](./logo-firefox.png)  | Firefox | Mozilla Foundation |
| ![](./logo-edge.png)     | Edge    | Microsoft          |
| ![](./logo-safari.png)   | Safari  | Apple              |

## Technologien

Für moderne Webseiten werden hauptsächlich drei Technologien verwendet:

- **HTML** (Hypertext Markup Language): Mit dieser Markup-Sprache wird die Struktur einer Webseite beschrieben. HTML-Dateien enthalten den «Bauplan» einer Webseite und Verweise auf alle anderen Dateien, welche für die Webseite benötigt werden.
- **CSS** (Cascading Stylesheets): Damit wir das Layout der Webseite definiert. Mit CSS werden Position, Farben, Animationseffekte und weitere Formatierungen der einzelnen Elemente der Webseite beschrieben.
- **JavaScript** ist eine Programmiersprache. Moderne Webseiten enthalten teilweise umfangreiche Programme, welche vom Browser ausgeführt werden.