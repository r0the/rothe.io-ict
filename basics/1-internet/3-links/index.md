# 1.3 Links / URLs
---

Ein Link oder URL (engl. *uniform resource locator*) ist eine globale, weltweit eindeutige [:fundamental: Adresse][1] für eine Datei. Die URL definiert, wie mit dem anderen Computer kommuniziert wird, wo dieser Computer zu finden ist, und welche Datei auf dem Computer gemeint ist. Die URL setzt sich aus vier Teilen zusammen:

![](./url.svg)

Das **Protokoll** ist die Übertragungsart der Datei, normalerweise `https://`, was bedeutet, dass die Übertragung verschlüsselt ist.

Der Name des **Servers** oder Domänenname ist ein weltweit eindeutiger Name für den Computer, auf welchem die Datei gespeichert ist.

Der **Dateipfad** ist eine eindeutige Bezeichnung einer Datei pro Computer.

Die **Suchanfrage** enthält Benutzereingaben, die auf interaktiven Webseiten erfasst werden.

::: exercise Aufgabe

Wie lauten die vier Teile des folgenden URL:

[https://www.google.ch/search?q=Quellenangabe][2]

:::

## Permalinks

Oft sind Links nicht lange gültig. Wenn beispielsweise eine Website restrukturiert wird, stimmen die Links oft nicht mehr. Einige Anbieter offerieren sogenannte «Permalinks», also permanente Links, welche langfristig funktionieren sollen.

- Auf «Der Bund» kann der Link [https://www.derbund.ch/digital/internet/das-urheberrecht-treibt-die-netzgemeinde-auf-die-strasse/story/24403218][11] so abgekürzt werden: [https://www.derbund.ch/24403218][12]
- Bei «NZZ» wird aus dem Link [https://www.nzz.ch/wirtschaft/youtube-haftet-kuenftig-fuer-urheberrechtsverletzungen-ld.1459750][13] das viel kürzere [https://www.nzz.ch/wirtschaft/ld.1459750][14]
- Der Youtube-Link [https://www.youtube.com/watch?v=NmM9HA2MQGI][15] wird zu [https://youtu.be/NmM9HA2MQGI][16]


::: exercise Aufgabe

Die folgenden Quellenangaben stammen aus [Korrektes Zitieren und Bibliographieren][21]. Die Links funktionieren aber nicht mehr. Versuche, den richtigen Link zu finden:

1. Goethe-Universität Frankfurt a. M.: Neurolinguistik (geändert am 6. November 2007) [http://www1.uni-frankfurt.de/fb/fb10/KogLi/Lehrstuehle/ehem__Lehrstuhl_Leuninger/Neurolinguistik/index.html][22] (abgerufen am 03.03.2018).

2. Baader, S.: Geometrie – Vorlesungsscript. Mitschrift von Alex Aeberli und Raoul Bourquin, Wintersemester 07. [PDF], Skript zur Vorlesung Geometrie an der ETHZ im Wintersemester 2007, (2007) [https://www.mitschriften.ethz.ch/main.php?page=3&scrid=1&pid=29&eid=1][23] (abgerufen am 31.01.2018).

3. ParlCH: «Erklär mir das Parlament». 18.02.2016 [https://www.youtube.com/watchv=vtfEG0txhOU][24] (abgerufen am 31.01.2018).

* [:link: Formular für Lösungen][31]
:::

[1]: ?context=/content/fundamentals/index&page=/content/fundamentals/address
[2]: https://www.google.ch/search?q=Quellenangabe

[11]: https://www.derbund.ch/digital/internet/das-urheberrecht-treibt-die-netzgemeinde-auf-die-strasse/story/24403218
[12]: https://www.derbund.ch/24403218
[13]: https://www.nzz.ch/wirtschaft/youtube-haftet-kuenftig-fuer-urheberrechtsverletzungen-ld.1459750
[14]: https://www.nzz.ch/wirtschaft/ld.1459750
[15]: https://www.youtube.com/watch?v=NmM9HA2MQGI
[16]: https://youtu.be/NmM9HA2MQGI

[21]: https://intern.gymkirchenfeld.ch/school/showDocument?id=60004
[22]: http://www1.uni-frankfurt.de/fb/fb10/KogLi/Lehrstuehle/ehem__Lehrstuhl_Leuninger/Neurolinguistik/index.html
[23]: https://www.mitschriften.ethz.ch/main.php?page=3&scrid=1&pid=29&eid=1
[24]: https://www.youtube.com/watchv=vtfEG0txhOU

[31]: https://docs.google.com/forms/d/e/1FAIpQLSfWmuYw2I7_VIqkgdXu47Z6492a4MlqH93LMpqa_933v4JoSA/viewform?usp=sf_link
