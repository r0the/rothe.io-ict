# 1.4 E-Mail verwenden
---

## Eine E-Mail ist ein Brief

Im Gegensatz zu einem Chat gelten bei der E-Mail-Kommunikation die gleichen Regeln wie beim Briefverkehr. Eine E-Mail enthält also eine **Anrede**, einen **höflichen** und **sprachlich korrekten** Text, sowie eine **Grussformel**.

Bei einer E-Mail wird immer auch ein **Betreff** angegeben. Dieser sollte so gewählt werden, dass er **kurz und prägnant** den Inhalt der E-Mail zusammenfasst.

::: info
*Betreff:* **Wechsel Schwerpunktfach**

Sehr geehrte Frau Schenk

In den ersten sechs Monaten hier am Gymnasium habe sich grosse Freude am Italienischunterricht bekommen. Das von mir gewählte Schwerpunktfach entspricht hingegen gar nicht meinen Vorstellungen. Deshalb frage ich Sie, ob es mir möglich wäre, in das Schwerpunktfach Italienisch zu wechseln.

Freundliche Grüsse

Anna Müller
:::

## An, Cc und Bcc

Beim Verfassen einer E-Mail können Empfänger in den drei Feldern **An**, **Cc** und **Bcc** angegeben werden.

- **An:** In diesem Feld werden Empfänger:innen angegeben, an welche die E-Mail gerichtet ist.
- **Cc:** Hier werden Empfänger:innen angegeben, welche die E-Mail als Kopie zur Information erhalten. Das bedeutet, dass in Cc erwähnte Empfänger:innen nichts weiter tun müssen, als die E-Mail zu lesen.
- **Bcc:** Hier können Empfänger:innen angegeben werden, welche die E-Mail «im Geheimen» erhalten sollen. Die hier angegebenen E-Mail-Adressen werden unterdrückt und können von niemandem, der die E-Mail erhält eingesehen werden. Bcc sollte daher verwendet werden, wenn man eine E-Mail an viele Personen schickt, welche sich gegenseitig nicht kennen.

## Herkunft von Cc und Bcc

**Cc** ist eine Abkürzung des Englischen Begriffs *carbon copy*, **Bcc** steht für *blind carbon copy*. Damit werden mit Kohlepapier angefertigte Kopien von Briefen bezeichnet.

Bei handgeschriebenen oder mit der Schreibmaschine erstellten Briefen war es üblich, ein Kohlepapier zu verwenden, um eine Kopie des Briefes zu erhalten, ohne diesen zweimal schreiben bzw. tippen zu müssen.

![Mit einem Kohlepapier erzeugte Kopie ©](./carbon-copy.jpg)

## Anrede

Die übliche Andere ist «Sehr geehrte/r Frau/Herr». Sind mehrere Personen angesprochen, schreibt man «Sehr geehtre Damen und Herren». Wenn die angesprochene Person unbekannt ist (z.B. bei einer Support-E-Mail-Adresse), so kann man «Guten Tag» schreiben.

Ist man mit der angeschriebenen Person vertrauter, schreibt man «Liebe/r».

Am Schluss der Anrede steht **kein Satzzeichen**. Es folgt eine Leerzeile, dann der erste Satz.

## Grussformel

Zwischen Text und Grussformel befindet sich eine Leerzeile. Die Grussformel lautet «Freundliche Grüsse» ohne Satzzeichen. Ist man vertrauter, schreibt man «Liebe Grüsse».

## Wann antworten? Wann nicht?

Benutze die Antworten-Funktion nur, wenn du wirklich eine Antwort schreibst. So kann eine längere E-Mail-Konversation sauber dargestellt und nachvollzogen werden.

Wenn du jemandem eine neue Nachricht schreibst, dich also nicht auf vorhergehenden E-Mail-Verkehr beziehst, dann solltest du die Antworten-Funktion nicht verwenden, sondern einfach eine neue E-Mail schreiben.

## Allen Antworten

Die Funktion _allen Antworten_ sollte man normalerweise nicht benutzen.
