# 2 Sicherheit
---

> Sicherheit bezeichnet einen Zustand, der **frei von unvertretbaren Risiken** ist oder als **gefahrenfrei** angesehen wird.
>
> Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Sicherheit)

* [2.1 Datenverlust](?page=1-data-loss/)
* [2.2 Authentifizierung](?page=2-authentication/)
* [2.3 Sichere Passwörter](?page=3-secure-passwords/)
* [2.4 Schadsoftware](?page=4-malicious-software/)

::: warning
### Massnahmen gegen Datenverslust
1. Sichere Passwörter verwenden.
2. Einen modernen Webbrowser mit neuesten Updates verwenden.
3. Regelmässig die neuesten Betriebssystem-Updates installieren.
4. Dateien auf einer Cloud (z.B. OneDrive) speichern.
:::