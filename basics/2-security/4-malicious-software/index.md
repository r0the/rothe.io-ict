# 2.4 Schadsoftware
---

Schadsoftware oder Schadprogramm (auch **Malware**) sind Computerprogramme, welche entwickelt wurden, um aus Sicht der Betroffenen unerwünschte und schädliche Funktionen auszuführen.

## Bezeichnungen

**Computerviren** sind Programme, welche sich selbständig und automatisch auf möglichst viele Geräte kopieren.

**Trojaner** sind Schadprogramme, welche sich als andere App, beispielsweise als Spiel tarnen.

**Ransomware** sind Schadsoftware, welche die Daten auf den infizierten Geräten verschlüsseln und für deren Entschlüsselung ein Lösegeld verlangen.

**Spyware** sind Schadprogramme, welche den Benutzer ausspionieren, indem sie Daten und Aktivitäten am Gerät an Dritte weiterleiten.

![WannaCry-Ransomware (2017) ©](./wanna-cry.png)

## Elk Cloner

Elk Cloner gilt als das erste Computervirus. Es wure 1982 vom damals fünfzehnjährigen Rich Skrenta geschrieben für den Apple II-Computer geschrieben. Es ware in einem Computerspiel versteckt, hat sich im Hauptspeicher des Computers eingenistet und sich auf alle Disketten kompiert, welche in den Computer eingelegt wurden.

![Elk Cloner ©](./elk-cloner.png)

## Einfallswege

Das Ziel von Schadsoftware ist es, Programmcode auf dem Gerät der Opfer auszuführen. Es gibt mehrere Wege, wie Programme auf das Gerät gelangen können.

![Einfallswege für Schadsoftware](./attack-vectors.svg)

### Via Webbrowser

Moderne Webseiten enthalten Programmcode. Dieser wird vom Webbrowser ausgeführt. Dabei verhindert der Browser, dass ohne Erlaubnis der Benutzers auf Dateien oder beispielsweise die Kamera auf dem lokalen Gerät zugegriffen werden kann.

Wie alle Software haben Webbrowser Sicherheitslücken, durch welche bösartige Programme auf Webseiten sich trotzdem Zugriff erschleichen können. Deshalb ist es wichtig einen bekannten, modernen Webbrowser zu verwenden und immer die neuesten Updates zu installieren.

### Via Store

Software Stores (z.B. Microsoft Store, App Store von Apple, Play Store von Google) unterziehen die angebotene Software einer Prüfung. Es gibt aber immer wieder Hinweise, dass Schadsoftware durch diese meist automatisierte Prüfung schlüpfen kann.

Benutzer können für Apps, welche über einen Store installiert werden, die Berechtigungen einstellen. So kann auf Windows, iOS und Android z.B. festgelegt werden, welche Apps auf Kamera, Fotos oder die Kontakte zugreifen dürfen.

Diese Berechtigungen werden vom Betriebssystem verwaltet, welches natürlich auch Sicherheitslücken hat. Deshalb sollte auch das Betriebssystem immer auf dem neuesten Stand gehalten werden.

### Via Installation

Auf Windows und macOS können von einer beliebigen Quelle heruntergeladene Programme installiert werden. Diese Programme erhalten bei der Installation weitreichende Rechte. Windows weist bei der Installation mit folgendem Hinweis darauf hin:

![](./install-sumatra-pdf.png)

Damit das Programm installiert werden kann, muss hier mit __Ja__ geantwortet werden. Das sollte man aber nur tun, wenn man sich sicher ist, dass das Programm aus einer vertrauenswürdigen Quelle stammt.