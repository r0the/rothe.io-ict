# 2.1 Datenverlust
---

Eine oft unterschätzte Gefahr ist der Verlust von Daten.

## Ursachen

::: columns 2
![Datenverlust durch Defekt des Geräts ©](./destroyed-phone.jpg)
:::

Für den Verlust von Daten gibt es verschiedene Ursachen, beispielsweise:

- Defekt des Geräts, auf welchem die Daten gespeichert sind,
- Verlust des Geräts, auf welchem die Daten gespeichert sind,
- Zerstörung der Daten durch Schadsoftware,
- Verschlüsselung der Daten durch Schadsoftware.

## Massnahmen

### Cloudspeicher verwenden

Daten sollten nie auf nur einem Gerät gespeichert werden. Mit Cloud-Lösung werden Daten automatisch zwischen mehreren Geräten und einem Cloudspeicher synchronisiert. Damit stehen die Daten auch noch zu Verfügung, wenn ein Gerät verloren geht oder einen Defekt hat.

### Schutz vor Schadsoftware

Mit einfachen [Massnahmen](?4-malicious-software/) kann man sich vor Schadsoftware schützen.
